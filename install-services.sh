#!/usr/bin/env bash

# This installs things are a "user service", which means it doesn't
# require you to be root when launching. It also means that certain
# variables are properly defined, certain things are properly set up.
# Timer itself doesn't require any particular services but chances are
# commands you add to timers will, and the program you use to plas
# alarms will. Like, MPV uses PipeWire, or Pulseaudio, and those
# require D-Bus. So use a user service. They're better, probably,
# however, you need to enable "linger", which you can do with
# loginctl: "loginctl enable-linger <YOU>". Without that, the services
# won't run until you log in, which is pretty useless for a timer
# that's meant to be run headlessly.

cd $(dirname $0)
typeset dir=${XDG_CONFIG_HOME-$HOME/.config/systemd}/user
mkdir -p $dir
doit() {
	basename=$1
	echo cp -i "$basename" "$dir"
	cp -i "$basename" "$dir"
	systemctl --user enable --now "$basename"
}

doit timer.service
doit timer-cmd-runner.service
