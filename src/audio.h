#pragma once

#include "macros.h"

constexpr char DEFAULT_TUNE_BASE_BASENAME[] = "default";

enum {
	AUDIO_ERR_BUF_COUNT = 1024,
	AUDIO_SAMPLE_RATE = 48000,
	AUDIO_MAX_MSGS = 64,
};

typedef struct audio_playing_tune audio_playing_tune_s;
struct audio_playing_tune {
	struct audio_tune *tune;
	bool loop;
	u32 frame;
};
typedef struct audio_playing_tune_arr audio_playing_tune_arr_s;
struct audio_playing_tune_arr {
	audio_playing_tune_s *d;
	int n, cap;
};

enum audio_cmd : u8 {
	AC_STOP_PLAYING_TUNES = 1,
	AC_QUIT,
	AC_ADD_TO_PLAYING,
};
typedef enum audio_cmd audio_cmd;

typedef struct audio_msg audio_msg_s;
struct audio_msg {
	audio_cmd cmd;
	union {
		char *str;
	};
};

typedef struct audio_tune audio_tune_s;
struct audio_tune {
	char *path;
	float *buf;
	int n_frames;
};

typedef struct audio_tune_arr audio_tune_arr_s;
struct audio_tune_arr {
	audio_tune_s *d;
	int n, cap;
};

typedef struct audio_ctx audio_ctx_s;
struct audio_ctx {
	audio_playing_tune_arr_s playing_tunes;
	audio_tune_arr_s tunes;
	int n_channels;
	bool valid;
	audio_msg_s msgs[AUDIO_MAX_MSGS];
	int n_msgs;
	void *pw;
	char err_buf[AUDIO_ERR_BUF_COUNT];
};

nod audio_ctx_s *audio_init (char *app_name, int sample_rate, int n_channels);
nod int audio_queue_add_to_playing (audio_ctx_s *ctx, char *tune_path);
nod int audio_queue_stop_playing_tunes (audio_ctx_s *ctx);
nod int audio_queue_quit (audio_ctx_s *ctx);
