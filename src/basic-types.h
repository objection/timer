#pragma once

#include <unistd.h>

typedef struct int_arr int_arr_s;
struct int_arr {
	int *d;
	ssize_t n, cap;
};

