#define _GNU_SOURCE
#include "cleanup.h"
#include <unistd.h>

void cleanup_fn_fd (void *arg) {
	close (*(int *) arg);
}
