#define _GNU_SOURCE

#include "filter.h"
#include "lib/str-to-int-arr/str-to-int-arr.h"
#include "misc.h"
#include "src/all.h"
#include "state.h"
#include "warn.h"
#include <stdarg.h>
#include <string.h> // IWYU pragma: keep
#include <regex.h>

thread_local static int CFD;
nod static int get_completed_list (int_arr_s *pr) {

	pr->n = 0;

	// Get an int list of all of the completed timers
	FORI (i, STATE->timers.n) {
		if (STATE->timers.d[i].flags & TF_completed)
			DARR_GROW_ADD (pr, 1, 1, i);
	}
	return 0;
}

nod static bool is_acceptable_error_if_no_match_is_not_an_error (enum s2ia_status s2ia_status) {
	if (
			(s2ia_status == S2IA_ERROR_MINUS_NUM_TAKEN_BELOW_ZERO ||
				s2ia_status == S2IA_ERROR_NUM_TOO_BIG)
			&& CLIENT_ARGS.no_match_is_not_an_error)
		return 1;
	return 0;
}

nod static err_e get_abs_int_list (int_arr_s *pr, char *s, int_arr_s current_ints,
		char *error_buf) {

	auto info = s2ia_str_to_int_arr (&pr->d, &pr->n, s, 0, STATE->timers.n, 0);
	if (info.status) {
		if (!is_acceptable_error_if_no_match_is_not_an_error (info.status)) {
			snprintf (error_buf, BUFSIZ, "%s", s2ia_error_strs[info.status]);
			return FAIL;
		}
	}

	// This is wasteful but fine. It means we realloc more than
	// necessary.
	pr->cap = pr->n;
	return SUCC;
}

nod static err_e get_rel_int_list (int_arr_s *pr, char *s, int_arr_s current_ints,
		char *error_buf) {
	struct s2ia_info info = s2ia_str_to_relative_int_arr (&pr->d, &pr->n, s,
			0, current_ints.d, current_ints.n, 0);
	if (info.status) {
		if (!is_acceptable_error_if_no_match_is_not_an_error (info.status)) {
			snprintf (error_buf, BUFSIZ, "%s", s2ia_error_strs[info.status]);
			return FAIL;
		}
	}

	// This is wasteful but fine. It means we will realloc more than
	// necessary.
	pr->cap = pr->n;
	return SUCC;
}

nod static int get_grep_list (int_arr_s *pr, char *s, char *error_buf) {
	pr->n = 0;
	regex_t preg;
	int rt = regcomp (&preg, s, 0);
	if (rt == -1) { regerror (rt, &preg, error_buf, BUFSIZ); return 1; }
	FORI (i, STATE->timers.n) {
		if (!regexec (&preg, STATE->timers.d[i].name, 0, 0, 0))
			DARR_GROW_ADD (pr, 1, 1, i);

		// Failing to match isn't a hard error
	}
	regfree (&preg);
	int r = 0;
	return r;
}

nod filter_s *get_token (filter_s *filter, filter_arr_s filters) {
	if (!filter)
		return filters.d;
	if (filter == filters.d + filters.n - 1)
		return 0;
	return filter + 1;
}

nod char *get_last_filter (filter_s *filter, filter_arr_s filters) {

	// Pretty dumb. Long and short is this will get the last filter,
	// or 0 if no last filter.

	struct filter *last =
		!filter ?
			filters.d + filters.n - 1 :
		filter == filters.d ?
			0 :
		filter - 1;
	if (!last)
		return 0;
	return last->opt_str;
}

static void filter_err (filter_s *filter, filter_arr_s filters, char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);

	int fd = CFD == -1 ? STDERR_FILENO : CFD;
	msg (&(mo_s) {"N", fd}, "%s: ", PROG_NAME);
	mo_s mo = {"NP", fd};
	if (!filter)
		msg (&mo, "at end: ");
	else
		msg (&mo, "in filter token %s: ", filter->opt_str);
	vdprintf (fd, fmt, ap);
	msg (&mo, "\n");
	return;
}

void filter_free_filters (filter_arr_s *filters) {
	SLICE_EACH (filter, filters)
		NUKE_PTR_IF (&filter->opt_str);
	free (filters->d);
	ZERO_MEM (filters);
}

nod int_arr_s do_filter (int _cfd, filter_arr_s filters) {
	CFD = _cfd;
	int_arr_s r = {};
	FORI (i, STATE->timers.n)
		DARR_GROW_ADD (&r, 1, 1, i);

	// If no filters, return a list of all timers.
	if (!filters.n)
		return r;
	char error_buf[BUFSIZ];
	int_arr_s list = {};

#define filter_err(_cfd, fmt, ...) ({ \
	filter_err (filter, filters, fmt, ##__VA_ARGS__); \
	filter_free_filters (&filters); \
	return (int_arr_s) {}; \
})
#define $get_token() get_token (filter, filters)
#define $get_last_filter() get_last_filter (filter, filters)

	filter_s *filter = 0;

	for (;;) {

		filter = $get_token ();
		if (!filter)
			break;
		if (filter == filters.d && filter->type == FT_OP && filter->op_type == OT_or)
			filter_err (CFD, "You can't start an expression with -o");

		op_type op_type;

		bool is_not = 0;

		if (filter->type == FT_LIST)
			op_type = OT_and;
		else {

			if (filter->op_type == OT_not) {
				is_not = 1;
				op_type = OT_and;
			} else
				op_type = filter->op_type;

			filter = $get_token ();
			if (!filter)
				filter_err (CFD, "A list needs to follow an operator (%s)",
						$get_last_filter ());
		}

		if (filter->type != FT_LIST) {
			if (filter->op_type == OT_not) {
				is_not = 1;

				filter = $get_token ();
				if (!filter)
					filter_err (CFD, "You can't end with %s", filter->opt_str);
			} else
				filter_err (CFD, "A list must be here");
		}

		switch (filter->list_type) {
		case LT_select:
			if (get_rel_int_list (&list, filter->str, r, error_buf))
				filter_err ("%s", error_buf);
			break;
		case LT_select_absolute:
			if (get_abs_int_list (&list, filter->str, r, error_buf))
				filter_err ("%s", error_buf);
			break;
		case LT_completed:
			if (get_completed_list (&list))

				// This isn't possible right now
				filter_err ("%s", error_buf);
			break;
		case LT_grep:
			if (get_grep_list (&list, filter->str, error_buf))
				filter_err ("%s", error_buf);
			break;
		}

		if (is_not)
			not_list (&list, r.d, r.n);

		switch (op_type) {
		case OT_and: and_list (&r, list.d, list.n, 1); break;
		case OT_or:  or_list (&r, list.d, list.n, 1);  break;
		case OT_not: ASSERT (false && "Op type wasn't and or or; should be impossible"); break;
		}

	}

	NUKE_PTR_IF (&list.d);

	return r;
}

