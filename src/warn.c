#define _GNU_SOURCE

#include "macros.h"
#include "warn.h"
#include "all.h"
#include <stdarg.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

void warn (char *fmt, ...) {
	va_list ap; va_start (ap, fmt);
	fprintf (stderr, "%s: ", PROG_NAME);
	vfprintf (stderr, fmt, ap);
	if (errno)
		fprintf (stderr, ": %s", strerror (errno));
	fputc ('\n', stderr);
	fflush (stderr);
	va_end (ap);
}

void msg (mo_s *msg_opts, char *fmt, ...) {
	va_list ap; va_start (ap, fmt);
	int fd = msg_opts && msg_opts->fd
		? msg_opts->fd
		: STDERR_FILENO;

	bool no_newline = false;
	bool dont_print_name = false;
	if (msg_opts && msg_opts->flags) {
		EACH_SENT (flag, msg_opts->flags) {
			switch (*flag) {
			case 'N': no_newline = 1; break;
			case 'P': dont_print_name = 1; break;
			default:  ASSERT (!"You've passed an invalid msg_opts flag");
			}
		}
	}

	FILE *f = fdopen (fd, "w");
	if (!dont_print_name)
		fprintf (f, "%s: ", PROG_NAME);
	vfprintf (f, fmt, ap);
	if (errno)
		fprintf (f, ": %s", strerror (errno));
	if (!no_newline)
		fputc ('\n', f);
	fflush (f);
	va_end (ap);
}

