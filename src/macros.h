#pragma once

#include <stdint.h>
#include <assert.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;
#define nod [[nodiscard]]
#define np nullptr
#define NUKE_PTR($ptr) \
	do { \
		free (*($ptr)); \
		*($ptr) = np; \
	} while (0)
#define NUKE_PTR_IF($what) do { if (*($what)) NUKE_PTR ($what); } while (0)
#define EACH($item, $n, $arr) \
	for (typeof (($arr)[0]) *($item) = ($arr); ($item) < ($arr) + ($n); ($item)++)
#define EACH_SENT($item, $arr) \
	for (typeof (($arr)[0]) *($item) = ($arr); *($item); ($item)++)
#define SLICE_EACH($item, $darr) \
	EACH ($item, ($darr)->n, ($darr)->d)
#define FORI($idx, $max) \
	for (typeof (($max) + 0) ($idx) = 0; $idx < ($max); ($idx)++)
#define FORI_SENT($idx, $arr) \
	for (ssize_t $idx = 0; ($arr)[$idx]; ($idx)++)
#define ASSERT assert
#define PLUS_ONE(...) + 1
#define SEL_1($first, ...) $first,
#define SEL_2($first, $second, ...) $second,
#define GROW($p, $n, $a, $amount_to_add, $incr_amount) \
	if (($n) + ($amount_to_add) >= *($a)) { \
		*($a) += ($incr_amount); \
		*($p) = realloc (*($p), *($a) * sizeof (typeof (**($p)))); \
		ASSERT (*$p); \
	}
#define ADD($p, $n, ...) \
	do { \
		($p)[(*($n))] = __VA_ARGS__; \
		*($n) += 1; \
	} while (0)
#define DARR_ADD($darr, ...) \
	ADD (($darr)->d, &($darr)->n, __VA_ARGS__)
#define	GROW_ADD($p, $n, $a, $amount_to_add, $incr_amount, ...) \
	do { \
		GROW ($p, *($n), $a, $amount_to_add, $incr_amount); \
		ADD (*($p), $n, __VA_ARGS__); \
	} while (0)
#define	GROW_ADD_EMPTY($p, $n, $a, $amount_to_add, $incr_amount) \
	GROW_ADD ($p, $n, $a, $amount_to_add, $incr_amount, (typeof ((*($p))[0])) {0})
#define	DARR_GROW_ADD_EMPTY($darr, $amount_to_add, $incr_amount) \
	GROW_ADD_EMPTY (&($darr)->d, &($darr)->n, &($darr)->cap, $amount_to_add, $incr_amount)
#define DARR_GROW_ADD($darr, $amount, $incr_amount, ...) \
	GROW_ADD (&($darr)->d, &($darr)->n, &($darr)->cap, $amount, $incr_amount, __VA_ARGS__)
#define REMOVE_FROM_ARR($array, $starting_from_idx, $n_to_remove, $max) \
	do { \
		memmove (&($array)[($starting_from_idx)], \
				&$array[($starting_from_idx) + ($n_to_remove)], \
				sizeof (typeof (*($array))) * \
				(*($max) - (($starting_from_idx) + ($n_to_remove)))); \
		(*($max))--;  \
	} while (0)
#define REMOVE_FROM_SLICE($slice, $starting_from, $n_to_remove) \
	REMOVE_FROM_ARR (($slice)->d, $starting_from, $n_to_remove, &($slice)->n)
#define ZERO_MEM(...) memset (__VA_ARGS__, 0, sizeof *(__VA_ARGS__))
#define countof(...) (sizeof (__VA_ARGS__) / sizeof (__VA_ARGS__)[0])
#define SLICE_LAST($arr) ($arr)->d[($arr)->n - 1]

