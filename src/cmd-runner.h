#pragma once

#include "macros.h"
#include "warn.h"

extern char *ALARM_CMD;
extern char *STOP_ALARM_CMD;
extern char *CMD_CMD;

nod err_e cmd_runner (bool daemonise);
