#pragma once

#include "macros.h"
#include "warn.h"
#include "str.h"
#include <sys/types.h>

typedef struct child child_s;
struct child {
	str_arr_s argv;
	pid_t pid;
	time_t end;
	int exit_status;
};

typedef struct child_arr child_arr_s;
struct child_arr {
	child_s *d;
	int n, cap;
};

void kill_all_children (child_arr_s *children);
nod err_e wait_for_children (struct child_arr *children);
