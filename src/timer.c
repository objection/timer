#define _GNU_SOURCE

#include "timer.h"
#include "state.h"
#include <string.h>

void splice_timer (int which, int n) {
	auto timers = &STATE->timers;
	memmove (timers->d + which,
			timers->d + which + n,
			(timers->n - which - n) * sizeof *timers->d);

	timers->n -= n;
}


