#pragma once

#include "macros.h"
#include "warn.h"
#include "str.h"
#include <stdlib.h>

nod err_e protocol_append_argv_to_fd (size_t argc, char **argv, int fd);
nod err_e protocol_cap_off_arg (int fd);
nod err_e protocol_get_argv_from_fd_stream (str_arr_s *pr, int fd);
void protocol_append_arg_to_fdf (int fd, char *fmt, ...);
