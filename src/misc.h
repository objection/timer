#pragma once

#include <stdlib.h>
#include "basic-types.h"
#include "str.h"

void remove_int_dups_inline (ssize_t *n_res, int **r);
void and_list (int_arr_s *pr, int *list2, size_t nlist2, bool rmdups);
void or_list (int_arr_s *pr, int *list2, size_t nlist2, bool rmdups);
void not_list (int_arr_s *buf, int *b, size_t nb);
void strip (char *s);
nod char *client_readlinef (int cfd, char *fmt, ...);
