#pragma once

#include "macros.h"

typedef struct str str_s;
struct str {
	char *d;
	int n, cap;
};

typedef struct str_arr str_arr_s;
struct str_arr {
	char **d;
	int n, cap;
};
void str_arr_nuke (str_arr_s *str_arr);
nod char *return_strs_as_static_string (str_arr_s *str_arr, bool quote);
