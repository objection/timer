#pragma once

#define CLEANUP($fn_tail) [[gnu::cleanup(cleanup_fn_ ## $fn_tail)]]
void cleanup_fn_fd (void *arg);
