#define _GNU_SOURCE

#include "client.h"
#include "warn.h"
#include "so-called-protocol.h"
#include "all.h"
#include "cleanup.h"
#include "socket.h"
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

thread_local client_args_s CLIENT_ARGS;

nod err_e client_run (int argc, char **argv) {
	char *addr = getenv ("TIMER_DEFAULT_SERVER_ADDR");
	CLEANUP (fd)
	int server_fd;
	if (client_connect_to_a_server (addr, SERVER_PORT, EXE_client, EXE_server,
			&server_fd))
		return FAIL;

	// Write argv to the server.
	if (protocol_append_argv_to_fd (argc, argv, server_fd))
		WARN_FAIL ("Couldn't write to socket");
	if (shutdown (server_fd, SHUT_WR))
		WARN_FAIL ("Couldn't close the write end of the server socket");

	char read_buf[BUFSIZ];
	int exit_code;

	// I've really struggled with writing out this data, making sure
	// it all gets written and making sure it can work in a pipe. Now
	// we just write it to a buffer and then write the whole thing out
	// at once. That seems to work.
	char *out = 0;
	size_t size = 0;
	FILE *f = open_memstream (&out, &size);
	if (!f)
		WARN_FAIL ("open_memstream failed");

	for (;;) {

		ssize_t n_bytes = read (server_fd, read_buf, sizeof read_buf);
		if (!n_bytes)
			break;
		else if (n_bytes == -1 && errno != EINTR) {
			fprintf (stderr, "Failed reading from server %s", addr);
			break;
		}
		// We keep setting exit_code to the last byte read. The exit
		// code is sent (as a char) as the last byte before closing
		// the socket. When the above read returns 0 we'll have the
		// exit code.
		exit_code = read_buf[n_bytes - 1];
		if (exit_code <= 1)
			n_bytes--;
		if (n_bytes)
			fwrite (read_buf, 1, n_bytes, f);
	}
	fclose (f);
	fwrite (out, 1, size, stdout);
	free (out);
	return exit_code;
}

nod static struct addrinfo *get_addrinfos (char *addr_str, int port) {

	struct addrinfo *addrinfo = 0;

	struct addrinfo hints = {

		// We should pass AF_UNSPEC, and support ipv6, but doing that
		// makes it so -Alocalhost doesn't work. I'm not sure why.
		// It's definitely my fault. But we'll just stick to ipv4 for
		// now.
		.ai_family = AF_INET,
		.ai_socktype = SOCK_STREAM,
	};

	char port_str[64];
	sprintf (port_str, "%d", port);
	auto rt = getaddrinfo (addr_str,

			// "service": if you leave it null it will remain
			// uninitialised. We'll initialise it in later.
			port_str, &hints, &addrinfo);
	if (rt)
		WARN_RET (np, "getaddrinfo failed: %s", gai_strerror (rt));
	return addrinfo;
}

nod err_e client_connect_to_a_server (char *addr_str, int port,
		exe_id_e from_server_id, exe_id_e to_server_id, int *server_fd) {

	if (!addr_str)
		addr_str = "127.0.0.1";
	else if (!strlen (addr_str))
		WARN_FAIL ("Your --addr is empty");

	auto addrinfos = get_addrinfos (addr_str, port);

	struct addrinfo *match;
	for (match = addrinfos; match; match = match->ai_next) {
		*server_fd = socket (match->ai_family, match->ai_socktype, match->ai_protocol);
		if (*server_fd != -1)
			break;
	}
	if (!match)
		WARN_FAIL ("Couldn't get an addrinfo for %s at %s (from %s)",
				EXE_NAMES[to_server_id], addr_str, EXE_NAMES[from_server_id]);

	int rc = connect_with_timeout (*server_fd, match->ai_addr, match->ai_addrlen, 1000);
	if (rc == -1)
		WARN_FAIL ("Couldn't connect to %s at %s (from %s)",
				EXE_NAMES[to_server_id], addr_str, EXE_NAMES[from_server_id]);

	freeaddrinfo (addrinfos);
	return SUCC;
}

