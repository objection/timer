#define _GNU_SOURCE

#include "child.h"
#include "state.h"
#include "str.h"
#include "all.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>

static void remove_child (child_arr_s *children, int idx) {
	auto child = children->d + idx;
	fprintf (stderr, "Job with pid %d, ", child->pid);
	SLICE_EACH (_, &child->argv) {
		if (*_) {

			// Might as well echo the args.
			fprintf (stderr, "\"%s\"", *_);
			if (_ != child->argv.d + child->argv.n - 1)
				fprintf (stderr, ", ");

			free (*_);
		}
	}

	free (child->argv.d);

	// Holy shit, don't forget this memset. The thing is the child
	// array is persistent, so its members might get reused. You need
	// to memset. Eh, is that true? Just don't take out this memset.
	// It's a load-bearing memset.
	memset (child, 0, sizeof *child);

	// Is it right to remove the PID? Who knows?
	REMOVE_FROM_ARR (children->d, idx, 1, &children->n);

	fprintf (stderr, " exited\n");
}

void kill_all_children (child_arr_s *children) {

	// This will brutally kill even the parent process. I think that's
	// fine. The only reason I think you might want to keep a child
	// process around is for commands. Perhaps the user has foolishly
	// decided to run a long-running command. But why are they killing
	// the server?
	/* int rt = killpg (0, SIGTERM); */
	/* if (rt) return msg (-1, "Couldn't kill children"); */

	FORI (i, children->n)
		remove_child (children, i);

	// I'm just not going to kill the children. We should also send a
	// message to the cmd-runners here, and stop any playing alarms.

	/* while (wait (0) != -1 || errno == EINTR); */
}

nod err_e wait_for_children (child_arr_s *children) {

	err_e r = SUCC;
	int wstatus = 0;

#define CHILD_WARN(fmt, ...) ({ \
	msg (&(mo_s) {.flags = "N"}, "%s: child \"%s\"", \
			PROG_NAME, \
			return_strs_as_static_string (&child->argv, 1)); \
	msg (np, fmt __VA_OPT__(,) __VA_ARGS__); \
})

	FORI (i, children->n) {
		auto child = children->d + i;
		int child_pid = waitpid (child->pid, &wstatus, WNOHANG);
		switch (child_pid) {
		case 0:
			if (child->end != -1 && STATE->now.ts >= child->end) {

				// Don't remove the child, here. It should
				// get cleared up the next time around.
				// 2023-03-09T18:07:59+00:00: truthfully I'm not
				// sure why I don't waitpid here. Well, it's not
				// like I want to block, so it does make sense to
				// leave it around for a minute.
				kill (child->pid, SIGTERM);
				remove_child (children, i);
				i--;
			}
			break;
		case -1:

			// If we're here it means the child has exited. It's
			// normal.
			if (errno && errno != ECHILD)
				CHILD_WARN (": Child error: %s", strerror (errno));
			remove_child (children, i);
			i--;
			errno = 0;
			break;

		default:
			if (!WIFEXITED (wstatus)) {
				CHILD_WARN (" returned abnormally: %d", WEXITSTATUS (wstatus));
				r = FAIL;
			} else if (WIFSIGNALED (wstatus)) {
				CHILD_WARN (" was stopped by signal %d", WTERMSIG (wstatus));
#ifdef WCOREDUMP
				if (WCOREDUMP (wstatus)) {
					CHILD_WARN (" dumped core");
					r = FAIL;
				}
#endif
			}
			remove_child (children, i);
			i--;
		}
	}

	return r;
}

