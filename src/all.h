#pragma once

#include "../lib/time-string/time-string.h"
#include "client.h"

#define SERVER_PORT 8884
#define CMD_RUNNER_PORT 8885

enum {
	MAX_HOSTS_TO_SEND_COMMANDS_TO = 16,
	DEFAULT_COMPLETED_TIMERS_RETENTION_DUR = TS_SECS_FROM_HOURS (2),
	CONNECT_TIMEOUT_MS = 1000,
	MAX_OPEN_SERVER_CONNECTIONS = 64,
};

extern char *PROG_NAME;
extern int MAIN_PID;
extern int CACHE_FD;

nod err_e make_enforce_single_instance (char *service_name, int *fd);
