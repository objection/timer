#define _GNU_SOURCE

#include "warn.h"
#include "state.h"
#include "args.h"
#include "timer.h"
#include "misc.h"
#include "all.h"
#include "filter.h"
#include <string.h>
#include <argp.h>

enum long_options {
	LO_prompt = 2000,
	LO_print_state,
	LO_no_daemonise,
	LO_no_match_is_not_an_error,
	LO_force,
	LO_ssh_client,
};
typedef enum long_options long_options;

typedef struct parser_vals parser_vals_s;
struct parser_vals {

	// These are actions, but they're not mutually-exclusive. You can
	// do them all at once, which won't always make sense. Note that
	// there's no PA_print. You only print if you've chosen no args.
	enum actions {
		PA_delete        = 1 << 1,
		PA_pause_unpause = 1 << 2,
		PA_add           = 1 << 3,
		PA_set_end       = 1 << 4,
		PA_set_name      = 1 << 5,
		PA_set_cmd       = 1 << 6,
		PA_lock          = 1 << 7,
		PA_print_state   = 1 << 8,
		PA_stop_alarm    = 1 << 9,
		PA_set_hosts     = 1 << 10,
		PA_repeat_tomorrow        = 1 << 11,
	} actions;
	enum flags {
		PVF_force                    = 1 << 0,
		PVF_prompt                   = 1 << 1,

		// Set when you've passed the first positional parameter and
		// nothing's gone wrong. If this is false, don't push the new
		// timer.
		PVF_timer_has_been_set       = 1 << 2,

		// Set when user does --hosts.
		PVF_user_has_specified_hosts = 1 << 3,

		// --ssh-client
		PVF_ssh_client               = 1 << 4,
	} flags;
	ts_dur add_dur;
	filter_arr_s filters;
	timer_s timer;
	enum position {
		POSITION_NONE,
		POSITION_TIME,
		POSITION_NAME,
	} position;
};

thread_local static int cfd;

nod static error_t parse_arg (int key, char *arg, struct argp_state *state);
struct argp client_argp = {
		.options = (struct argp_option[]) {

			{0, 0, 0, 0, "Modes"},

			{"be-client", '0', 0, 0, "\
Be the client. This is the default"},
			{"be-server", '1', 0, 0, "\
Be server. This needs to be running to add and track timers. Pass with --help for \
server options"},
			{"be-cmd-runner", '2', 0, 0,
"Be cmd-runner. This needs to be running locally to play alarms and run timer \
commands locally. Pass with --help for server options"},

			{0, 0, 0, 0, "Actions"},

			{"add", 'a', "DUR", 0,
				"\
Add DUR to timers specified by --select. Dur can be negative. See end for DUR"},
			{"cmd", 'c', "CMD", 0,
				"\
Add a cmd to run when the timer elapses. If you want to delete a timer but still \
run its cmd, set its --end to 0s: \"timer -gyour-timer -s0\""},
			{"delete", 'd', 0, 0, "Delete timers specified by --select"},
			{"end", 'e', "TIME STR",  0,
				"Set ends of timers specified by --select to TIME STR"},
			{"lock", 'l', 0,  0,
				"Make timers locked so they can't be modified or deleted without --force"},
			{"pause-unpause", 'P', 0, 0,
				"Toggle pause state in timers specified by --select"},
			{"repeat", 'R', 0, 0,
				"Make ends of timers specified by --select repeat"},
			{"name", 'n', "STRING",  0,
				"Set names of timers specified by --select to STRING"},
			/* {"print", 'p', 0, 0, */
			/* 	"\ */
/* Print timers specified by --select; default action, selected by specifying no \ */
/* other actions"}, */

			{0, 0, 0, 0, "Selection"},

			{"completed", 'C', 0, 0, "Select completed timers"},
			{"grep", 'g', "REGEX", 0,
				"Select timers by pattern matching. Flavour is PCRE"},
			{"or", 'o', 0, 0, "A logical or; use with filters. See end"},
			{"not", '!', 0,  0,
				"A logical not; used with filters. See end"},
			{"select-absolute", 'S', "INDEX LIST", 0,
				"Select timers by \"INDEX LIST\". See end"},
			{"select", 's', "INDEX LIST", 0,
				"\
Select a timer by, I dunno, \"RELATIVE INDEX LIST\". \
With nothing preceding it in a filter \
pipeline, this would be identical to -S, ie, \"timer -s-1\" would \
select the last timer. But in a \
filter pipeline, the the list is relative to the list already created. \
Example: \
\"timer -gtest -s-1\" would select the _last_ timer matching \"test\". See end \
for what I mean by \"INDEX LIST\""},

			{0, 0, 0, 0, "General"},

			{"addr", 'A', "ADDRESS", 0, "\
The address of the Timer server. By default \
it's the environment variable TIMER_DEFAULT_SERVER_ADDR, else 127.0.0.1"},
			{"be-client", '0', 0, OPTION_HIDDEN},
			/* {"follow", 'f', 0, 0, */
			/* 	"\ */
/* When printing, follow the timers. If not outputting to a tty, this does nothing"}, */
			/* {"follow-lines", 'L', 0, 0, */
			/* 	"\ */
/* When printing, follow the timers, except print a newline at the end of each"}, */
			{"force", LO_force, 0, 0,
				"\
Perform actions on locked timers"},
			{"hosts", 'h', "CSV",  0,
				"\
A comma-separated list of hosts to send the selected timers' commands and alarms. \
You don't usually need to set this because by default timers \
get one of these, the host you're \
using. Setting this will remove that, so if you want to run the commands on \
the computer you're in front of and another one you'll need to specify both"},
			{"kill", 'k', 0, 0,
				"Kill all timers. I'll probably remove this"},
			{"no-daemonise", LO_no_daemonise, 0, 0,
				"Don't daemonise, when running as the backend"},
			{"no-match-is-not-an-error", LO_no_match_is_not_an_error, 0, 0,
				"\
Don't return non-zero on no match. Just possibly you might use this to watch for \
a timer coming into existence"},
			{"prompt", LO_prompt, 0, 0,
				"\
When adding a new timer, prompt you for a name if you leave it out. \
Obviously intended to be used in an \
alias. If you just type return the timer will remain unnamed"},
			{"print-state", LO_print_state, 0, 0,
				"\
Print the server's state, stuff like the tune duration. Also, whether the timer's \
active or not"},
			{"retain-completed-for", 'r', "DUR", 0,
				"DURATION to retain completed timers for. By default they stay forever"},
			{"stop-alarms", 'x', 0, 0, "Stop all playing alarms"},
			{"ssh-client", LO_ssh_client, 0, 0, "\
Instead of the using the client's addr, use the value of its SSH_CLIENT \
variable, if there, else use localhost. Overridden by --hosts"},
			{"tune", 't', "CMDLINE", 0,
				"\
The tune to play. If blank, use a file whose basename without suffix is \"default\" \
in your data-dir, eg \"default.ogg\""},
			{"tune-duration", 'D', "DUR", 0,
				"How long to play the alarm for; if blank, just play the whole tune once"},
			{},
		},
		parse_arg,
		"<END-TIME> <name>", "\
A timer. Acts as server with -1, a \"cmd-runner\" with -2, else as client\v\
Timer needs at least a server to run (\"-1\" / \"--be-server\"). To play \
alarms and run timer commands on your local machine, you need the \
cmd-runner running \
(\"-2\" / \"--be-cmd-runner\"). Later on you'll be able to run commands on \
any host you can access and which has a cmd-runner running. To \
see help for any of the modes, add --help in addition the opts \
that start them.\n\
\n\
About INDEX LISTs: these describe the indexes of the things you're interested in. Eg:\n\
	\"0\" -> the first element\n\
	\"0,4\" -> the first and the fifth\n\
	\"0..4\" -> the first fifth\n\
	\"0..-1\" -> all of them. -1 means the last, -2 the second-last, etc\n\
\n\
About TIMEs: these are ISO dates like 2022-01-11T07:00:57 or DURs (see below). \
Most likely you'll be most often using durs or just the time part of the TIME, \
which you can do by prefixing the T: \"T14\" (2 in the afternoon); \
\"T14:30\" means half-two. Another thing you can do is, eg, \"2dT14\".\n\
\n\
About DURs: these are durations, like 2h20s. y: years; M: months; d: days; \
h: hours; m: minutes: s: seconds.\n\
\n\
About filters: -g, -s, and --completed are filters. You can combine these with --or \
and --not in the usual way. There's no --and, because and is operator used if \
--or is left out. You can't put expressions in brackets"
	};

nod static int check_name (char *arg) {
	if (strpbrk (arg, (char []) {' ', '\n', '\t', 0})) {
		msg (&(mo_s) {.fd = cfd}, "\
\"%s\" is bad; timer names can't have spaces. \
This is to make timer's output easier to chop up with cli programs", arg);
		return 1;
	}
	if (strlen (arg) >= TIMER_MAX_NAME) {
		msg (&(mo_s) {.fd = cfd}, "You name, \"%s\", is longer than the max, %d",
				arg, TIMER_MAX_NAME);
		return 1;
	}
	strip (arg);
	return 0;
}

static void print_state () {
	char tune_duration[64];
	ts_str_from_dur (tune_duration, 64, &(ts_dur) {.secs = STATE->tune_duration},
		 &STATE->now, 0);
	char completed_timers_retention_dur[64];
	ts_str_from_dur (completed_timers_retention_dur, 64,
			&(ts_dur) {.secs = STATE->completed_timers_retention_dur},
		 &STATE->now, 0);

	dprintf (cfd,

"Active:                        Yes\n\
Tune duration:                  %s\n\
Completed timers retention dur:  %s\n\
Data Dir:                       \"%s\"\n\
Tune:                           \"%s\"\n",

			tune_duration,
			completed_timers_retention_dur,
			STATE->data_dir,
			STATE->tune);
}

static void splice_multiple_timers (int *ints, size_t n_ints) {
	for (int i = n_ints - 1; i >= 0; i--)
		splice_timer (ints[i], 1);
}

static void set_cmds (char *cmd) {
	SLICE_EACH (_, &CLIENT_ARGS.selection)
		strncpy (STATE->timers.d[*_].cmd, cmd, TIMER_MAX_CMD);
}

static void delete_timers () {
	splice_multiple_timers (CLIENT_ARGS.selection.d, CLIENT_ARGS.selection.n);
}

static void pause_unpause_timers () {
	EACH (_, CLIENT_ARGS.selection.n, CLIENT_ARGS.selection.d) {
		if (!(STATE->timers.d[*_].flags & TF_completed))
			STATE->timers.d[*_].flags ^= TF_paused;
	}
}

static void add_to_timers (ts_dur dur) {

	EACH (_, CLIENT_ARGS.selection.n, CLIENT_ARGS.selection.d) {
		if (!(STATE->timers.d[*_].flags & TF_completed))
			STATE->timers.d[*_].end = add_dur_to_time (STATE->timers.d[*_].end, dur);
	}
}

static void set_timers (ts_time time) {
	EACH (_, CLIENT_ARGS.selection.n, CLIENT_ARGS.selection.d) {
		auto timer = STATE->timers.d + *_;
		if (!(timer->flags & TF_completed))
			timer->end = time;
	}
}

static void set_hosts (client_addr_arr_s *client_addrs) {
	EACH (_, CLIENT_ARGS.selection.n, CLIENT_ARGS.selection.d) {
		auto timer = STATE->timers.d + *_;
		if (!(timer->flags & TF_completed))

			// We just set the hosts. I can't be bothered with some
			// kind of add-hosts, remove-hosts.
			memcpy (&timer->client_addrs, client_addrs, sizeof *client_addrs);
	}
}

static void set_names (char *name) {
	EACH (_, CLIENT_ARGS.selection.n, CLIENT_ARGS.selection.d) {
		if (!(STATE->timers.d[*_].flags & TF_completed))
			memccpy (STATE->timers.d[*_].name, name, 0, TIMER_MAX_NAME);
	}
}

nod static err_e set_selection (parser_vals_s *parser_vals) {
	CLIENT_ARGS.selection = do_filter (cfd, parser_vals->filters);

	// Remove locked timers from selection
	if (
			// Ie, don't filter out locked items when we're printing.
			// --print happens when no other actions are chosen.
			parser_vals->actions) {

			/* // If you've chosen an action, we don't filter out locked */
			/* // items if you've also done --force */
			/* && !(parser_vals->flags & PVF_force)) { */
		FORI (i, CLIENT_ARGS.selection.n) {
			if (STATE->timers.d[CLIENT_ARGS.selection.d[i]].flags & TF_locked &&
					!(parser_vals->flags & PVF_force)) {
				REMOVE_FROM_SLICE (&CLIENT_ARGS.selection, i, 1);
				i--;
			}
		}
	}

	if (CLIENT_ARGS.selection.n == 0 && !CLIENT_ARGS.no_match_is_not_an_error)

		// We don't print an error message.
		return FAIL;
	return SUCC;
}

nod static int print_timer_selection (int cfd) {
	char remaining_buf[256];
	auto timers = &STATE->timers;
	if (!timers->n)
		return 0;

	EACH (index, CLIENT_ARGS.selection.n, CLIENT_ARGS.selection.d) {

		auto timer = &STATE->timers.d[*index];

		// We need to calculate this here, even though the backend
		// calculates it. We can't actually rely on that, because
		// the backend sleeps for a second.
		ts_dur remaining = {.secs = timer->end.ts - STATE->now.ts};
		ts_str_from_dur (remaining_buf, 256, &remaining, &STATE->now, 0);
		if (ts_errno) {
			msg (&(mo_s) {.fd = cfd}, "ts_string_from_duration failed: %s",
					ts_strerror (ts_errno));
			return 1;
		}
		dprintf (cfd, "%zu %s %s%s%s%s", timer - timers->d, timer->name,
				remaining_buf, (timer->flags & TF_repeat_tomorrow) ? " [R]" : "", (timer->flags & TF_completed) ? " COMPLETE" : "",
				timer->flags & TF_locked ? " locked" : "");
		if (*timer->cmd)
			dprintf (cfd, "; cmd: \"%s\"", timer->cmd);
		if (timer->client_addrs.n) {
			dprintf (cfd, "; hosts: ");
		   	FORI (i, timer->client_addrs.n) {
				dprintf (cfd, "%s", timer->client_addrs.d[i]);
				if (i != timer->client_addrs.n - 1)
					dprintf (cfd, ",");
			}
		}
		dprintf (cfd, "\n");
	}

	return 0;
}

nod static err_e print (int cfd, parser_vals_s *parser_vals) {
	/* for (;;) { */
#define $run_oe(_cmd) ({ r = _cmd; if (r) break; })

		// If it's not --follow (ie, it's -p), don't print terminal
		// control characters.
#if 0
#define p_if_follow(fmt, ...) \
		do { if (CLIENT_ARGS.print_mode == PM_FOLLOW) \
			dprintf (cfd, fmt, ##__VA_ARGS__); } while (0)
#endif

		// The only place a client sticks around is
		// when you --follow. So we loop. And we need
		// to re-get the selection each time, since
		// timers might get added and deleted.
		if (set_selection (parser_vals))
			return FAIL;
		if (CLIENT_ARGS.selection.n) {
			/* p_if_follow ("[2K"); */
			// DEDGEC.
			(void) print_timer_selection (cfd);
			/* p_if_follow ("\x1b[%zuA", CLIENT_ARGS.selection.n); */
		} /* else if (CLIENT_ARGS.print_mode == PM_FOLLOW_LINES)
			dprintf (cfd, "\n");
		*/

		/* if (CLIENT_ARGS.print_mode == PM_PRINT) */
		/* 	return 0; */
		/* sleep (1); */
	/* } */
	return 0;

}
nod static err_e set_end (ts_time *end, char *arg, char *token,

		// When calling this as a result of parsing the first
		// positional, it's an error to give a time of less than
		// now. But when calling it as a result of "-e", it's not.
		// That's because you can use -e to stop a timer and run
		// its cmd. If you used -d, the cmd wouldn't run.
		bool accept_minus) {
	auto new_end = ts_time_from_str (arg, 0, &STATE->now, 0);
	if (ts_errno) {
		msg (&(mo_s) {.fd = cfd}, "\"%s\": %s", arg, ts_strerror (ts_errno));
		return FAIL;
	}
	if (!accept_minus && new_end.ts <= STATE->now.ts) {
		msg (&(mo_s) {.fd = cfd}, "%s is <= now", arg);
		return FAIL;
	}
	*end = new_end;
	return SUCC;
}

static void lock () {
	SLICE_EACH (_, &CLIENT_ARGS.selection)
		STATE->timers.d[*_].flags |= TF_locked;
}

static void stop_alarms () {
	SLICE_EACH (_, &CLIENT_ARGS.selection)
		STATE->timers.d[*_].flags |= TF_stop_alarm;
}

static void repeat () {
	SLICE_EACH (_, &CLIENT_ARGS.selection)
		STATE->timers.d[*_].flags |= TF_repeat_tomorrow;
}

nod static error_t parse_arg (int key, char *arg, struct argp_state *argp_state) {

	// It means we've done --help. When you do --help in the
	// timer we don't bother going to the server, and all that's valid
	// is --help.
	static bool did_client_help = 0;
	if (cfd == -1) {
		if (!did_client_help) {
			argp_state_help (argp_state, stdout, ARGP_HELP_LONG);
			/* argp_usage (argp_state); */
			did_client_help = 1;
		}
		return SUCC;
	}
	parser_vals_s *parser_vals = argp_state->input;
	int r = FAIL;

#define ADD_FILTER(...) ({ \
	DARR_GROW_ADD (&parser_vals->filters, 1, 1, __VA_ARGS__); \
	parser_vals->filters.d[parser_vals->filters.n - 1].opt_str = strdup (argp_state->argv[argp_state->next - 1]); \
})
	switch (key) {
	case LO_force:
		parser_vals->flags |= PVF_force;
		break;
	case LO_no_match_is_not_an_error:
		CLIENT_ARGS.no_match_is_not_an_error = true;
		break;
	case LO_print_state:
		parser_vals->actions |= PA_print_state;
		break;
	case LO_prompt:
		parser_vals->flags |= PVF_prompt;
		break;
	case LO_ssh_client:
		ASSERT (0);
		break;
	case ARGP_KEY_INIT:
		break;
	case ARGP_KEY_ARG:

		if (parser_vals->position == POSITION_NONE) {
			if (set_end (&parser_vals->timer.end, arg, "time positional", 0))
				break;
			parser_vals->flags |= PVF_timer_has_been_set;
		} else if (parser_vals->position == POSITION_TIME) {
			if (check_name (arg))
				break;
			strncpy (parser_vals->timer.name, arg, sizeof parser_vals->timer.name);
		} else {
			argp_usage (argp_state);
			goto out;
		}

		parser_vals->position++;
		break;
	case ARGP_KEY_ERROR:

		// Do basically nothing. This gets called when this
		// function returns non-zero, ie, when you indicate
		// there's been an error. I don't know if there's a way to
		// just stop parsing right at that moment.
		//
		// FIXME? The argp info page says this function should
		// return ARGP_SOMETHING_UNKNOWN
		// if this particular parsing function doesn't deal with a
		// particular option or else some Unix error code. I can't
		// be bothered, but you could do "if (errno) return
		// errno;", but if you did that, you'd couldn't return, as
		// I am here, 1 to indicate an error, because on is EPERM.

		break;
	case ARGP_KEY_END:

		if (parser_vals->actions & PA_print_state) {
			print_state ();
			break;
		}
		// Whether you're adding a note or changing them is
		// decided by whether you've used a positional parameter
		// or not.
		if (parser_vals->position == 0) {

			if (!parser_vals->actions) {
				if (print (cfd, parser_vals))
					goto out;
			}

			if (set_selection (parser_vals))
				goto out;

			// There's no logic restricting you from using certain
			// combinations. There was, but there's no point.
			// Nothing wrong with doing -a and -e together, say.
			if (parser_vals->actions & PA_set_cmd)       set_cmds (parser_vals->timer.cmd);
			if (parser_vals->actions & PA_delete)        delete_timers ();
			if (parser_vals->actions & PA_pause_unpause) pause_unpause_timers ();
			if (parser_vals->actions & PA_add)           add_to_timers (parser_vals->add_dur);
			if (parser_vals->actions & PA_set_end)       set_timers (parser_vals->timer.end);
			if (parser_vals->actions & PA_set_hosts)     set_hosts (&parser_vals->timer.client_addrs);
			if (parser_vals->actions & PA_set_name)      set_names (parser_vals->timer.name);
			if (parser_vals->actions & PA_lock)          lock ();
			if (parser_vals->actions & PA_stop_alarm)    stop_alarms ();
			if (parser_vals->actions & PA_repeat_tomorrow)        repeat ();

		} else if ((parser_vals->flags & PVF_prompt) && parser_vals->position == POSITION_TIME) {
			char *name = client_readlinef (0, "Enter timer name: ");
			if (!check_name (name))
				break;
			strncpy (parser_vals->timer.name, name, TIMER_MAX_NAME);
			free (name);
		}

		if (parser_vals->flags & PVF_timer_has_been_set) {
			if (STATE->timers.n >= MAX_TIMERS)
				WARN_OUT ("Too many timers (%d); max: %d", STATE->timers.n, MAX_TIMERS);

			if (!*parser_vals->timer.name)
				strncpy (parser_vals->timer.name, UNNAMED_TIMER_NAME, TIMER_MAX_NAME);
			STATE->timers.d[STATE->timers.n++] = parser_vals->timer;
		}

		break;
	case '!':
		ADD_FILTER ((filter_s) {.type = FT_OP, .op_type = OT_not});
		break;
	case 'A':

		// This has already been got, by the client, in main.
		break;
	case 'a':
		parser_vals->actions |= PA_add;
		parser_vals->add_dur = ts_dur_from_dur_str (arg, 0);
		if (ts_errno)
			WARN_OUT ("add: \"%s\": %s", arg, ts_strerror (ts_errno));
		break;
	case 'C':
		ADD_FILTER ((filter_s) {.type = FT_LIST, .list_type = LT_completed});
		break;
	case 'c':
		parser_vals->actions |= PA_set_cmd;
		strncpy (parser_vals->timer.cmd, arg, sizeof parser_vals->timer.cmd);
		break;
	case 'd':
		parser_vals->actions |= PA_delete;
		break;
	case 'D':
		STATE->tune_duration = ts_secs_from_dur (ts_dur_from_dur_str (arg, 0),
				&STATE->now);
		if (ts_errno)
			WARN_OUT ("tune-duration: \"%s\": %s", arg, ts_strerror (ts_errno));
		break;
	case 'e':
		parser_vals->actions |= PA_set_end;
		if (set_end (&parser_vals->timer.end, arg, "-e", 1))
			goto out;
		break;
#if 0
	case 'f':

		// If you're not outputting to a file, the way timer
		// prints and removes and prints is a bit pointless. So
		// just disable following. An alternative would be to in
		// that case print each tick on a new line. I don't right
		// now see the point in that.
		if (!isatty (STDOUT_FILENO))
			CLIENT_ARGS.print_mode = PM_FOLLOW;
		break;
#endif
	case 'g':
		ADD_FILTER (((filter_s) {.str = arg, .type = FT_LIST,
					.list_type = LT_grep}));
		break;
	case 'h':
		if (!(parser_vals->flags & PVF_user_has_specified_hosts)) {

			// When you specify --hosts, the default host (the one
			// you're at) gets replaced with the ones you specify.

			parser_vals->timer.client_addrs.n = 0;
			parser_vals->flags |= PVF_user_has_specified_hosts;
		}
		else if (parser_vals->timer.client_addrs.n >= MAX_HOSTS_TO_SEND_COMMANDS_TO)
			WARN_FAIL ("You've specified too many --hosts; max: %d",
					MAX_HOSTS_TO_SEND_COMMANDS_TO);
		char *ip;
		while (ip = strsep (&arg, ","))

			// Use that function that always null-terminates?
			strncpy (parser_vals->timer.client_addrs.d[parser_vals->timer.client_addrs.n++],
					ip, sizeof parser_vals->timer.client_addrs.d[0]);
		parser_vals->actions |= PA_set_hosts;
		break;
	case 'k':
		STATE->kill = 1;
		break;
	case '4':

		// Do nothing; dealt with in main.
		break;
#if 0
	case 'L':
		CLIENT_ARGS.print_mode = PM_FOLLOW_LINES;
		break;
#endif
	case 'l':
		parser_vals->actions |= PA_lock;
		parser_vals->timer.flags |= TF_locked;
		break;
#if 0
	case 'p':
		CLIENT_ARGS.print_mode = PM_PRINT;
		break;
#endif
	case 'P':
		parser_vals->actions |= PA_pause_unpause;
		break;
	case 'r':
		STATE->completed_timers_retention_dur =
			ts_secs_from_dur (ts_dur_from_dur_str (arg, 0), &STATE->now);
		if (ts_errno)
			WARN_OUT ("\"%s\": %s", arg, ts_strerror (ts_errno));
		break;
	case 'R':
		parser_vals->actions |= PA_repeat_tomorrow;
		parser_vals->timer.flags |= TF_repeat_tomorrow;
		break;
	case 'o':
		ADD_FILTER ((filter_s) {.type = FT_OP, .op_type = OT_or});
		break;
	case 'n':
		parser_vals->actions |= PA_set_name;
		if (check_name (arg))
			goto out;
		snprintf (parser_vals->timer.name, 0xff, "%s", arg);
		break;
	case 'S':
		ADD_FILTER ((filter_s) {.str = arg, .type = FT_LIST, .list_type = LT_select_absolute});
		break;
	case 's':
		ADD_FILTER ((filter_s) {.str = arg, .type = FT_LIST, .list_type = LT_select});
		break;
	case 'x':
		parser_vals->actions |= PA_stop_alarm;
		break;
	case 't':
		if (strlen (arg) >= PATH_MAX)
			WARN_OUT ("You're trying to make your tune longer than %d", PATH_MAX);
		strncpy (STATE->tune, arg, countof (STATE->tune));
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	r = SUCC;
out:

	return r;
}

void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET)
		return &(((struct sockaddr_in*)sa)->sin_addr);

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

nod int set_ip_str_from_sockaddr (char pr[static INET6_ADDRSTRLEN]) {

	struct sockaddr_in client_sockaddr = {};
	socklen_t client_sockaddr_len = sizeof client_sockaddr;

	// The truth is we already know the client's sockaddr. It's got
	// in deal_with_client. So if you care you could get the
	// client_addr string there. Or you could pass the client sockaddr
	// itself around.
	int rc = getpeername (cfd, &client_sockaddr, &client_sockaddr_len);
	if (rc)
		return rc;
	if (!inet_ntop (client_sockaddr.sin_family,
			(struct socketaddr *) &client_sockaddr.sin_addr,
			pr, INET6_ADDRSTRLEN))
		return -1;
	return 0;
}

nod err_e do_args (int argc, char **argv, int _cfd) {

	cfd = _cfd;

	parser_vals_s parser_vals = {};

	if (_cfd != -1) {
		if (set_ip_str_from_sockaddr (parser_vals.timer.client_addrs.d[0]))
			WARN_FAIL ("Couldn't get client address: %m");
		parser_vals.timer.client_addrs.n++;
	}

	int stdout_orig = _cfd == -1 ? -1 : dup (STDOUT_FILENO);
	int stderr_orig = _cfd == -1 ? -1 : dup (STDERR_FILENO);

	if (cfd != -1) {
		int it = dup2 (cfd, STDERR_FILENO);
		int it_2 = dup2 (cfd, STDOUT_FILENO);
		if (it == -1 || it_2 == -1)
			WARN_FAIL ("Couldn't dup file descriptors: %m");
	}

	if (argp_parse (&client_argp, argc, argv, ARGP_NO_EXIT /* | ARGP_NO_ERRS */, 0,
				&parser_vals))
		return FAIL;

	filter_free_filters (&parser_vals.filters);
	NUKE_PTR_IF (&CLIENT_ARGS.selection.d);

	if (_cfd != -1) {
		dup2 (stdout_orig, STDOUT_FILENO);
		if (close (stdout_orig))
			WARN_FAIL ("Couldn't close child stdout file descriptor");
		dup2 (stderr_orig, STDERR_FILENO);
		if (close (stderr_orig))
			WARN_FAIL ("Couldn't close child stderr file descriptor");
	}

	return SUCC;
}

