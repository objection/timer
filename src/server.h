#pragma once

#include "macros.h"
#include "warn.h"

nod err_e server_run (bool daemonise);
nod err_e write_to_cache_file ();
nod err_e create_server_socket (int port, int *fd);
nod int server_accept_client (int server_fd);
