#define _GNU_SOURCE

#include "misc.h"
#include "macros.h"
#include "warn.h"
#include "lib/readlinef/readlinef.h"
#include <stdarg.h>

int cmp_ints (const void *a, const void *b) {
	return *(int *) a - *(int *) b;
}

void remove_int_dups_inline (ssize_t *n_res, int **r) {

	// obviously no need to do anything it it's 0 or 1 long
	if (*n_res == 0 || *n_res == 1)
		return;

	int saved_res[*n_res];
	int nsaved_res = *n_res;

	memcpy (saved_res, *r, nsaved_res * sizeof *saved_res);
	qsort (saved_res, *n_res, sizeof saved_res[0], cmp_ints);

	*n_res = 0;
	int *q = *r;

	int this = *saved_res;
	*q++ = this;
	(*n_res)++;
	for (int *p = saved_res; p - saved_res < nsaved_res; p++) {
		if (*p != this) {
			this = *p;
			*q++ = this;
			(*n_res)++;
		}
	}
	ASSERT (*n_res);
}

void and_list (int_arr_s *pr, int *list2, size_t nlist2, bool rmdups) {

	int_arr_s tmp = {};

	for (size_t i = 0; i < pr->n; ++i) {
		bool seen = 0;
		for (size_t j = 0; j < nlist2; ++j) {
			if (list2[j] == pr->d[i]) {
				seen = 1;
				break;
			}
		}
		if (seen)
			DARR_GROW_ADD (&tmp, 1, 1, pr->d[i]);
	}

	if (pr->d)
		free (pr->d);
	*pr = tmp;

	if (rmdups)
		remove_int_dups_inline (&pr->n, &pr->d);
}

void or_list (int_arr_s *pr, int *list_2, size_t n_list_2, bool rm_dups) {
	pr->d = realloc (pr->d, sizeof pr->d[0] * pr->n);
	memcpy (pr->d + pr->n, list_2, sizeof list_2[0] * n_list_2);
	pr->n += n_list_2;

	if (rm_dups)
		remove_int_dups_inline (&pr->n, &pr->d);
}

void not_list (int_arr_s *buf, int *b, size_t nb) {

	int_arr_s tmp = {};

	FORI (i, nb) {
		int match = 0;
		FORI (j, buf->n) {
			if (buf->d[j] == b[i])
				match = 1; //if flag=1 the elemnt is in A and in B
		}
		if (!match)
			DARR_GROW_ADD (&tmp, 1, 1, b[i]);
	}
	free (buf->d);
	*buf = tmp;
}

void strip (char *s) {
	char *p = s;
	int len = strlen (p);
	if (len == 0) return;

	while (isspace (p[len - 1])) p[--len] = 0;
	while (*p && isspace (*p))
		++p, --len;

	memmove (s, p, len + 1);
}

nod char *client_readlinef (int cfd, char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);

	FILE *client_f = fdopen (cfd, "r");
	if (!client_f) {
		msg (&(mo_s) {.fd = cfd}, "Couldn't make FILE * from fd");
		return 0;
	}
	FILE *prev_readline_f = rl_instream;
	rl_instream = client_f;
	char *r = readlinef_ap (0, fmt, ap);
	va_end (ap);
	rl_instream = prev_readline_f;
	return r;
}

