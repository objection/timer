#pragma once

#include "timer.h"
#include <limits.h>

typedef struct state state_s;
struct state {
	timer_arr_s timers;
	struct ts_time now;
	time_t tune_duration;
	time_t completed_timers_retention_dur;
	char data_dir[PATH_MAX];
	char tune[PATH_MAX];
	bool kill;
};

extern state_s *STATE;
