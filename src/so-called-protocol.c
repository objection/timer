#define _GNU_SOURCE

#include "so-called-protocol.h"
#include "macros.h"
#include "str.h"
#include "../lib/n-fs/n-fs.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

nod err_e protocol_cap_off_arg (int fd) {
	return write (fd, (char [1]) {'\0'}, sizeof (char)) == -1;
}

void protocol_append_arg_to_fdf (int fd, char *fmt, ...) {
	va_list ap;
	va_start (ap);
	ASSERT (vdprintf (fd, fmt, ap) != -1);
	va_end (ap);
	ASSERT (protocol_cap_off_arg (fd) != -1);
}

nod err_e protocol_append_argv_to_fd (size_t argc, char **argv, int fd) {
	FORI (i, argc) {
		if (write (fd, argv[i], strlen (argv[i]) + 1) == -1)
			return FAIL;
	}
	return SUCC;
}

nod err_e protocol_get_argv_from_fd_stream (str_arr_s *pr, int fd) {

	size_t n_r;
	char *buf = n_fs_read_all (&n_r, fd);
	if (!buf)
		return FAIL;

	memset (pr, 0, sizeof *pr);

	char *zero = buf;
	char *_ = buf;

	while (zero - buf < n_r && (zero = strchr (zero, 0))) {
		DARR_GROW_ADD (pr, 1, 1, strdup (_));
		zero++;
		_ = zero;
	}
	DARR_GROW_ADD (pr, 1, 1, 0);
	pr->n--;

	free (buf);
	return SUCC;
}

