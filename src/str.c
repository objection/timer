#define _GNU_SOURCE

#include "macros.h"
#include "str.h"
#include <stdio.h>
#include <stdlib.h> // IWYU pragma: keep

void str_arr_nuke (str_arr_s *str_arr) {
	SLICE_EACH (arg, str_arr)
		NUKE_PTR_IF (&arg);
	NUKE_PTR_IF (&str_arr->d);
}

nod char *return_strs_as_static_string (str_arr_s *strs, bool quote) {
	static char buf[BUFSIZ] = {};
	FILE *f = fmemopen (buf, sizeof buf, "w");
	SLICE_EACH (_, strs) {
		if (quote) fprintf (f, "%s", *_);
		else	   fprintf (f, "\"%s\"", *_);
		if (_ - strs->d != strs->n - 1)
			fprintf (f, " ");
	}
	fclose (f);
	buf[BUFSIZ - 1] = 0;
	return buf;
}

