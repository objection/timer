#pragma once

#include "../lib/time-string/time-string.h"
#include <arpa/inet.h>

constexpr int TIMER_MAX_CMD = 1024 * 4;
constexpr int TIMER_MAX_NAME = 32;
constexpr int TIMER_MAX_CLIENTS = 32;
constexpr int MAX_TIMERS = 64;
constexpr char UNNAMED_TIMER_NAME[] = "UNNAMED";

typedef char client_addr [INET6_ADDRSTRLEN];
typedef struct client_addr_arr client_addr_arr_s;
struct client_addr_arr {
	client_addr d[TIMER_MAX_CLIENTS];
	int n, cap;
};

enum timer_flags {
	TF_completed =       1 << 0,
	TF_paused =          1 << 1,
	TF_locked =          1 << 2,
	TF_stop_alarm =      1 << 3,
	TF_repeat_tomorrow = 1 << 4,
};
typedef enum timer_flags timer_flags_e;

typedef struct timer timer_s;
struct timer {
	struct ts_time end;
	char cmd[TIMER_MAX_CMD];
	char name[TIMER_MAX_NAME];
	client_addr_arr_s client_addrs;
	timer_flags_e flags;
};

struct timer_arr {
	timer_s d[MAX_TIMERS];
	int n;
};
typedef struct timer_arr timer_arr_s;
void splice_timer (int which, int n);
