#define _GNU_SOURCE

#include "audio.h"
#include "warn.h"
#include <sndfile.h>
#include <samplerate.h>
#include <spa/param/audio/format-utils.h>
#include <pipewire/pipewire.h>

#define err_msg(_ctx, fmt, ...) \
		snprintf ((_ctx)->err_buf, sizeof (_ctx)->err_buf, fmt __VA_OPT__(,) __VA_ARGS__); \

typedef struct pipewire_stuff pipewire_stuff;
struct pipewire_stuff {
	struct pw_thread_loop *thread_loop;
	struct pw_stream *stream;
};

static void audio_callback (void *userdata);
static struct pw_stream_events stream_events = {
	PW_VERSION_STREAM_EVENTS,
	.process = audio_callback
};

static inline void fill_audio_buffer(void *user, float *buf, u32 n_frames) {
    audio_ctx_s *ctx = user;
    memset (buf, 0, n_frames * sizeof (float) * ctx->n_channels);

	if (!ctx->playing_tunes.n)
		return;

	SLICE_EACH (playing_tune, &ctx->playing_tunes) {

		int sample_start = playing_tune->frame * ctx->n_channels;
		for (int i = 0; i < n_frames * ctx->n_channels; i++) {
			if (playing_tune->frame + i < playing_tune->tune->n_frames)
				buf[i] += playing_tune->tune->buf[sample_start + i];
		}
		playing_tune->frame += n_frames;
	}

	// This is to keep the volume from getting too loud. It's not
	// actually a good enough solution since it makes the volume
	// quieter and quieter the more tracks you add.
	for (int i = 0; i < n_frames * ctx->n_channels; i++)
		buf[i] /= ctx->playing_tunes.n;
}

// ChatGPT.
nod static int convert_tunes_sample_rate (audio_tune_s *tune, float *in_buf,
		int n_channels, long input_frames, int out_sr, int in_sr) {
	tune->n_frames = (long) (((float) input_frames / in_sr) * out_sr);
	float *r = calloc (tune->n_frames * n_channels, sizeof (float));

    SRC_DATA src_data;
    int error;

    // Initialize the src_data struct
    src_data.data_in = in_buf;
    src_data.data_out = r;
    src_data.input_frames = input_frames;
    src_data.output_frames = tune->n_frames;
    src_data.src_ratio = (float) out_sr / (float) in_sr;

    // Create a sample rate converter
    SRC_STATE *src_state = src_new (SRC_SINC_BEST_QUALITY, 1, &error);

    // Perform sample rate conversion
    src_process (src_state, &src_data);
	tune->buf = src_data.data_out;
	tune->n_frames = src_data.output_frames;

    // Free the sample rate converter
    src_delete (src_state);
	return 0;
}

nod static int read_in_tune (audio_tune_s *tune, SNDFILE *sndfile, int n_channels,
		SF_INFO sf_info) {

	int size_in_frames = sf_seek (sndfile, 0, SF_SEEK_END);
	ASSERT (size_in_frames != -1);
	sf_seek (sndfile, 0, SF_SEEK_SET);
	float *unconverted_buf = calloc (size_in_frames * n_channels,
			sizeof *unconverted_buf);

	ASSERT (sf_readf_float (sndfile, unconverted_buf, size_in_frames) != -1);

	if (sf_info.samplerate != AUDIO_SAMPLE_RATE) {

		if (convert_tunes_sample_rate (tune, unconverted_buf, n_channels, sf_info.frames,
				AUDIO_SAMPLE_RATE, sf_info.samplerate))
			return FAIL;
		free (unconverted_buf);
	} else {
		tune->buf = unconverted_buf;
		tune->n_frames = sf_info.frames;
	}
	return SUCC;
}

nod static err_e create_tune (audio_ctx_s *ctx, char *path, audio_tune_s *out) {
	SF_INFO sf_info = {};
	SNDFILE *sndfile = sf_open (path, SFM_READ, &sf_info);
	if (!sndfile) {
		err_msg (ctx, "Won't play the tune %s because we can't open it: %s\n",
				path, sf_strerror (0));
		return FAIL;
	}
	ZERO_MEM (out);
	out->path = strdup (path);
	if (read_in_tune (out, sndfile,

				// We're ignoring the number of channels. I'm not
				// sure what to do with them.
				ctx->n_channels, sf_info))
		return FAIL;
	ASSERT (out->buf);
	sf_close (sndfile);
	return SUCC;
}

nod static err_e add_to_playing (audio_ctx_s *ctx, audio_msg_s *msg) {
	audio_playing_tune_s playing_tune = {};
	char *tune_path = msg->str;
	SLICE_EACH (tune, &ctx->tunes) {
		if (!strcmp (tune->path, tune_path)) {
			playing_tune.tune = tune;
			break;
		}
	}

	if (!playing_tune.tune) {

		audio_tune_s new_tune;
		if (create_tune (ctx, tune_path, &new_tune))
			return FAIL;

		DARR_GROW_ADD (&ctx->tunes, 1, 1, new_tune);
		playing_tune.tune = &SLICE_LAST (&ctx->tunes);
	}

	DARR_GROW_ADD (&ctx->playing_tunes, 1, 1, playing_tune);
	return SUCC;
}

static void tune_nuke (audio_tune_s *tune) {
	free (tune->path);
	ZERO_MEM (tune);
}

static void free_audio_ctx (audio_ctx_s *ctx) {
	SLICE_EACH (tune, &ctx->tunes)
		tune_nuke (tune);
	NUKE_PTR_IF (&ctx->tunes.d);
	NUKE_PTR_IF (&ctx->playing_tunes.d);

	pipewire_stuff *pw = ctx->pw;

	pw_stream_destroy (pw->stream);

	pw_thread_loop_destroy (pw->thread_loop);

}

static void quit (audio_ctx_s *ctx) {
	pw_thread_loop_stop (((pipewire_stuff *) ctx->pw)->thread_loop);
	free_audio_ctx (ctx);
	pw_deinit ();
}

static void audio_callback (void *userdata) {

	audio_ctx_s *ctx = userdata;

	int n_msgs = ctx->n_msgs;
	ctx->n_msgs = 0;
	EACH (msg, n_msgs, ctx->msgs) {

		switch (msg->cmd) {
		case AC_STOP_PLAYING_TUNES: ctx->playing_tunes.n = 0; break;
		case AC_QUIT:               quit (ctx); return;
		case AC_ADD_TO_PLAYING:     (void) add_to_playing (ctx, msg); break;
		}
	}

	pipewire_stuff *pw = ctx->pw;
	auto pw_buf = pw_stream_dequeue_buffer (pw->stream);
	if (!pw_buf) {
		pw_log_warn ("out of buffers: %m");
		return;
	}

	auto spa_buf = pw_buf->buffer;
	u8 *p = spa_buf->datas[0].data;
	if (!p)
		return;

	// According to the PipeWire tutorial you can change sample rate
	// and channel and format here. I'm not sure what you'd change.
	// Anyway, here we can't use that to change the sample rate, since
	// we're mixing signals and so need them to match.

	int stride = sizeof (float) * ctx->n_channels;
	int n_frames = SPA_MIN (pw_buf->requested, spa_buf->datas[0].maxsize / stride);

	fill_audio_buffer (ctx, (float *) p, n_frames);

	FORI (i, ctx->playing_tunes.n) {
		auto playing_tune = ctx->playing_tunes.d + i;
		if (playing_tune->frame >= playing_tune->tune->n_frames) {
			if (playing_tune->loop) {
				playing_tune->frame = 0;
				continue;
			}
			ctx->playing_tunes.d[i] = ctx->playing_tunes.d[--ctx->playing_tunes.n];
			i--;
		}
	}

	spa_buf->datas[0].chunk->offset = 0;
	spa_buf->datas[0].chunk->stride = stride;
	spa_buf->datas[0].chunk->size = n_frames * stride;

	pw_stream_queue_buffer (pw->stream, pw_buf);
}

nod static int queue_msg (audio_ctx_s *ctx, audio_msg_s msg) {

	// If we max out our messages we just ignore the new ones.
	// Probably it'd be better to shift them along?
	if (ctx->n_msgs == AUDIO_MAX_MSGS - 1) {
		err_msg (ctx, "Too many messages. Ignoring this one");
		return 1;
	}
	ctx->msgs[ctx->n_msgs++] = msg;
	return 0;
}

// We send a message because of how resampling the tune can take a
// moment. If it takes too long it holds up cmd-runner, which makes
// the server wait, which makes timer clients wait, which makes them
// time out.
nod int audio_queue_add_to_playing (audio_ctx_s *ctx, char *tune_path) {
	return queue_msg (ctx, (audio_msg_s) {
		.cmd = AC_ADD_TO_PLAYING,
		.str = tune_path,
	});
}

nod int audio_queue_quit (audio_ctx_s *ctx) {
	return queue_msg (ctx, (audio_msg_s) {.cmd = AC_QUIT});
}

nod int audio_queue_stop_playing_tunes (audio_ctx_s *ctx) {
	return queue_msg (ctx, (audio_msg_s) {.cmd = AC_STOP_PLAYING_TUNES});
}

nod audio_ctx_s *audio_init (

		// We're pretending this is a module or library. To be honest,
		// it is that. But if you wanted to play some tunes you'd be
		// better off using MPV or FFMPEG.
		char *app_name, int sample_rate, int n_channels) {

	audio_ctx_s *r = calloc (1, sizeof *r);

	r->n_channels = n_channels;

	pw_init (0, 0);

	r->pw = calloc (1, sizeof (pipewire_stuff));
	pipewire_stuff *pw = r->pw;
	pw->thread_loop = pw_thread_loop_new ("Timer alarm", 0);

	uint8_t buffer[1024];
	auto pod_builder = SPA_POD_BUILDER_INIT (buffer, sizeof (buffer));

	auto props = pw_properties_new (PW_KEY_MEDIA_TYPE, "Audio",
			PW_KEY_MEDIA_CATEGORY, "Playback", PW_KEY_MEDIA_ROLE, "Music",
			NULL);
	if (!props) {
		err_msg (r, "Couldn't add properties");
		goto out;
	}

	pw->stream = pw_stream_new_simple (pw_thread_loop_get_loop (pw->thread_loop),
			"Timer alarm", props,
			&stream_events, r);
	if (!pw->stream) {
		err_msg (r, "Couldn't create stream");
		goto out;
	}

	const struct spa_pod *params = spa_format_audio_raw_build (&pod_builder,
			SPA_PARAM_EnumFormat, &(struct spa_audio_info_raw) {
				.format = SPA_AUDIO_FORMAT_F32,
				.channels = r->n_channels,
				.rate = sample_rate
			});
	if (!params) {
		err_msg (r, "Couldn't turn params into a spa_pod");
		goto out;
	}

	if (pw_stream_connect (pw->stream, PW_DIRECTION_OUTPUT, PW_ID_ANY,
			PW_STREAM_FLAG_AUTOCONNECT | PW_STREAM_FLAG_MAP_BUFFERS, &params, 1)) {
		err_msg (r, "Couldn't connect to stream");
		goto out;
	}

	// This just returns -1 on error. If it fails it means it didn't
	// create the thread.
	if (pw_thread_loop_start (pw->thread_loop))
		err_msg (r, "Couldn't start the PipeWire loop");

	r->valid = 1;

out:
	if (!r->valid)

		// Just hoping Pipewire will just free everything. Who knows
		// what needs to be freed and by which function.
		quit (r);
	return r;
}
