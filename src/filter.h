#pragma once

#include "macros.h"
#include "basic-types.h"

// This name is wrong. "Lists" and op aren't "filter types".
enum filter_type : u8 {
	FT_LIST,
	FT_OP,
};
typedef enum filter_type filter_type;

enum list_type : u8 {
	LT_select,
	LT_select_absolute,
	LT_grep,
	LT_completed,
};
typedef enum list_type list_type;

enum op_type : u8 {
	OT_and,
	OT_or,
	OT_not,
};
typedef enum op_type op_type;


typedef struct filter filter_s;
struct filter {
	char *str;
	char *opt_str;
	enum filter_type type;
	union {
		enum op_type op_type;
		enum list_type list_type;
	};
};
typedef struct filter_arr filter_arr_s;
struct filter_arr {
	filter_s *d;
	int n, cap;
};

nod int_arr_s do_filter (int _cfd, filter_arr_s filter);
void filter_free_filters (filter_arr_s *filters);
