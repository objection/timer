#pragma once

#include "macros.h"
#include "exe-mode.h"
#include "warn.h"
#include "stdio.h"
#include "basic-types.h"

typedef struct client_args client_args_s;
struct client_args {

	/* enum args_flags flags; */
	char *shortcut_tune;
	int sfd;
	FILE *s_f;

	int cfd;
	/* enum print_mode print_mode; */

	bool no_match_is_not_an_error;
	int_arr_s selection;
};

extern thread_local client_args_s CLIENT_ARGS;

nod err_e client_run (int argc, char **argv);
nod err_e client_connect_to_a_server (char *addr_str, int port, exe_id_e from_server_id,
		exe_id_e to_server_id, int *server_fd);
