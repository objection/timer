#pragma once

#include "macros.h"
#define EXE_DEFS($x) \
	$x (EXE_client,     "client") \
	$x (EXE_server,     "server") \
	$x (EXE_cmd_runner, "cmd-runner")

enum exe_id : u8 {
	EXE_DEFS (SEL_1)
};
constexpr int EXE_N = 0 EXE_DEFS (PLUS_ONE);
typedef enum exe_id exe_id_e;

extern char *EXE_NAMES[EXE_N];

