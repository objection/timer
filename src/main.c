#define _GNU_SOURCE

#include "all.h"
#include "server.h"
#include "cmd-runner.h"
#include "args.h"
#include "client.h"
#include <assert.h>
#include <string.h>
#include <argp.h>

enum prog_mode {
	PM_CLIENT,
	PM_SERVER,
	PM_CMD_RUNNER
};
typedef enum prog_mode prog_mode;

nod static error_t parse_server_arg (int key, char *arg, struct argp_state *argp_state) {

	bool *daemonise = argp_state->input;
	switch (key) {
	case 'D': *daemonise = 0; break;
	}
	return 0;
}

nod static char *get_ip_from_ssh_client_var () {
	char *var = getenv ("SSH_CLIENT");
	if (!var)
		return 0;
	var = strdup (var);
	char *space = strchr (var, ' ');
	ASSERT (space);
	*space = 0;

	return var;
}

nod static error_t parse_mode_selector_arg (int key, char *arg,
		struct argp_state *argp_state) {
	prog_mode *prog_mode = argp_state->input;
	switch (key) {
	case ARGP_KEY_INIT: *prog_mode = PM_CLIENT; break;
	case '0':           *prog_mode = PM_CLIENT; break;
	case '1':           *prog_mode = PM_SERVER; break;
	case '2':           *prog_mode = PM_CMD_RUNNER; break;
	}
	return 0;
}

nod int main (int argc, char **argv) {

	PROG_NAME = argv[0];
	MAIN_PID = getpid ();
	struct argp server_argp = {
		.options = (struct argp_option []) {
			{"be-server",    '1', 0, OPTION_HIDDEN},
			{"no-daemonise", 'D', 0, 0,              "Run in the foreground"},
			{"no-daemonize", 'D', 0, OPTION_ALIAS,   0},
			{},
		},
		// Used for cmd_runner, too.
		parse_server_arg,
		0, "Timer's server"
	};

	struct argp cmd_runner_argp = {
		.options = (struct argp_option []) {
			{"be-cmd-runner", '2', 0, OPTION_HIDDEN},
			{"no-daemonise",  'D', 0, 0,              "Run in the foreground"},
			{"no-daemonize",  'D', 0, OPTION_ALIAS,   0},
			{},
		},
		parse_server_arg,
		0, "A server that runs Timer cmds and plays alarms"
	};

	prog_mode prog_mode;
	argp_parse (&(struct argp) {
			(struct argp_option []) {
			{"be-client",     '0', 0,         OPTION_HIDDEN},
			{"be-server",     '1', 0,         OPTION_HIDDEN},
			{"be-cmd-runner", '2', 0,         OPTION_HIDDEN},
			{"addr",          'A', "ADDRESS", OPTION_HIDDEN},
			{},
			}, parse_mode_selector_arg, 0, 0,
		}, argc, argv, ARGP_NO_ERRS, 0, &prog_mode);
	// Need to check if we're the server first.

	bool daemonise = true;
	switch (prog_mode) {
	case PM_CLIENT:
		EACH_SENT (_, argv) {
#define arg_is(_what) (!strcmp (*_, _what))
			if (arg_is ("--help") || (**_ == '-' && strchr (*_, '?'))
					|| arg_is ("--usage")) {
				if (do_args (argc, argv, -1))
					exit (FAIL);
				exit (SUCC);
			} else if (arg_is ("--ssh-client")) {
				char *local_ip = get_ip_from_ssh_client_var ();
				if (local_ip) {
					*_ = local_ip;
					asprintf (_, "--hosts=%s", local_ip);
				}
				free (local_ip);
			}
#undef arg_is
		}
		if (client_run (argc, argv))
			exit (FAIL);
		break;
	case PM_SERVER:
		argp_parse (&server_argp, argc, argv, 0, 0, &daemonise);
		if (server_run (daemonise))
			exit (FAIL);
		break;
	case PM_CMD_RUNNER:
		argp_parse (&cmd_runner_argp, argc, argv, 0, 0, &daemonise);
		if (cmd_runner (daemonise))
			exit (FAIL);
		break;
	}

	exit (SUCC);
}



