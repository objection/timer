#define _GNU_SOURCE

#include "warn.h"
#include "audio.h"
#include "args.h"
#include "all.h"
#include "so-called-protocol.h"
#include "state.h"
#include "timer.h"
#include "server.h"
#include "cmd-runner.h"
#include "child.h"
#include "lib/n-fs/n-fs.h"
#include <stdarg.h>
#include <poll.h>
#include <signal.h>
#include <errno.h>
#include <limits.h>
#include <glob.h>
#include <threads.h>
#include <sys/mman.h>

static child_arr_s CHILDREN;
static int SERVER_FD;
static char SHM_FILE_PATH_TAIL[PATH_MAX];
static int LOCK_FILE_PID;
static thrd_t MAIN_THREAD;
thrd_t CHECK_TIMERS_THREAD;

// FIXME (if you like): Takes only buf, no n_buf Buf should be
// PATH_MAX.
static void get_data_dir (char *buf) {
	char *data_home = getenv ("XDG_DATA_HOME");
	if (!data_home) {
		data_home = n_fs_get_full_path ("~/.local/share");
		snprintf (buf, PATH_MAX, "%s/timer", data_home);
		free (data_home);
	} else
		snprintf (buf, PATH_MAX, "%s/timer", data_home);
}

static void print_args (int n_args, char *args[n_args]) {
	mo_s mo = {"PN"};
	msg (&mo, "%s: ", PROG_NAME);
	for (int i = 0; i < n_args; i++) {
		msg (&mo, "\"%s\"", args[i]);
		if (i != n_args - 1)
			msg (&mo, ", ");
	}
	msg (&mo, "\n");
}

nod static int send_message_to_cmd_runner (char *addr, ...) {

	// Feel free to make this MAX_ARGS or dynamically allocate.
	char *cmd_args[64];
	int n_cmd_args = 0;

	va_list ap;
	va_start (ap);
	for (;;) {
		char *arg = va_arg (ap, char *);
		if (!arg)
			break;
		cmd_args[n_cmd_args++] = arg;
	}

	va_end (ap);

#define informative_error_message(fmt, ...) \
	do { \
		msg (&(mo_s) {"N"}, "Server %s, port %d: " fmt "; cmd: ", addr, \
				CMD_RUNNER_PORT, ##__VA_ARGS__); \
		print_args (n_cmd_args, cmd_args); \
	} while (0)

	int cmd_runner_fd;
	if (client_connect_to_a_server (addr, CMD_RUNNER_PORT, EXE_server,
			EXE_cmd_runner, &cmd_runner_fd))
		return 1;

	FORI (i, n_cmd_args) {
		protocol_append_arg_to_fdf (cmd_runner_fd, cmd_args[i]);
	}
	ASSERT (protocol_cap_off_arg (cmd_runner_fd) != -1);

	int rc = shutdown (cmd_runner_fd, SHUT_WR);
	if (rc) {
		informative_error_message ("Couldn't shut down the server's fd (%d)", cmd_runner_fd);

		// Head in the sand.
		return 0;
	}
	if (close (cmd_runner_fd)) {
		informative_error_message ("Couldn't close the server's fd (%d)", cmd_runner_fd);

		// Head in the sand.
		return 0;
	}

	return 0;
}

nod static int do_alarm (timer_s *timer, char *addr) {
	char tune_duration_str[256];
	snprintf (tune_duration_str, sizeof tune_duration_str, "%ld",
			STATE->tune_duration);

	return send_message_to_cmd_runner (addr, ALARM_CMD, timer->name,
			tune_duration_str, STATE->tune, 0);
}

nod static int exec_cmd (timer_s *timer, char *addr) {
	return send_message_to_cmd_runner (addr, CMD_CMD, timer->cmd, 0);
}

nod static int for_all_cmd_runners (timer_s *timer, int (*do_what) (struct timer *, char *)) {
	int n_successes = 0;
	FORI (i, timer->client_addrs.n) {

		// Call this defensive programming. It shouldn't be necessary,
		// but just leave it.
		if (!*timer->client_addrs.d[i])
			continue;
		int rc = do_what (timer, timer->client_addrs.d[i]);
		if (!rc)
			n_successes++;
	}
	if (!n_successes)
		return 1;
	return 0;
}

nod static int stop_alarm (timer_s *, char *addr) {
	return send_message_to_cmd_runner (addr, STOP_ALARM_CMD, 0);
}

nod static int perform_client_op (int cfd, str_arr_s *client_argv) {
	int r = 1;

	int shm_fd = shm_open (SHM_FILE_PATH_TAIL, O_RDWR, 0);
	if (shm_fd == -1)
		WARN_FAIL ("In child, couldn't open shared memory");
	STATE = mmap (0, sizeof *STATE, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);

	if (STATE == MAP_FAILED) {
		msg (&(mo_s) {.fd = cfd}, "Couldn't mmap shared memory");
		goto out;
	}
	if (close (shm_fd)) {
		msg (&(mo_s) {.fd = cfd}, "Couldn't close shared memory file descriptor");
		goto out;
	}

	r = do_args (client_argv->n, client_argv->d, cfd);
	if (r)
		msg (np, "child %s failed with %d",
			return_strs_as_static_string (client_argv, 1), r);

	// This is the "exit code". It's a char so it won't split in half
	// on the other end. Note the sizeof (char). This is perfectly
	// acceptable behaviour. It's fine here, anyway. I can see why
	// people use JSON and co, though.

	write (cfd, (char []) {r}, sizeof (char));

	// Note we're not really dealin gwiht this stuff.
	int rc = shutdown (cfd, SHUT_WR);
	if (rc)
		msg (np, "Couldn't shut down client fd %d. It may stick around", cfd);

	// Might not even need to close this, since the child is about
	// to exit.
	rc = close (cfd);
	if (rc)
		msg (np, "Couldn't close client with file descriptor %d", cfd);

	// This is a bug. That r = deal_with_cmds up there gets ignored.
	// The fix is all the cleanup stuff above should happen below the
	// out: label.
	r = 0;
out:
	// I suppose we should close this? Else it will block if the
	// child is still alive?
	if (close (SERVER_FD))
		msg (np, "Couldn't close server socket");

	return r;
}

nod static err_e deal_with_client () {

	int client_fd = server_accept_client (SERVER_FD);

	str_arr_s client_argv = {};
	int r = protocol_get_argv_from_fd_stream (&client_argv, client_fd);
	if (r) {
		msg (np, "Couldn't read anything from client");
		goto out;
	}

	shutdown (client_fd, SHUT_RD);

	int child_pid = fork ();
	if (child_pid == -1) {
		msg (np, "Couldn't fork to deal with client");
		goto out;

	} else if (child_pid == 0) {
		msg (&(mo_s) {"N"}, "Job with pid %d, ", getpid ());
		mo_s mo = {"NP"};
		SLICE_EACH (_, &client_argv) {
			if (*_) {
				msg (&mo, "\"%s\"", *_);
				if (_ != client_argv.d + client_argv.n - 1)
					msg (&mo, ", ");
			}
		}
		msg (&mo, " started\n");
		close (CACHE_FD);
		close (LOCK_FILE_PID);
		int client_r = perform_client_op (client_fd, &client_argv);
		exit (client_r);
	}

	DARR_GROW_ADD (&CHILDREN, 1, 1,
			(child_s) {
				.pid = child_pid,
				.argv = client_argv,
				.end = -1
			});

out:
	if (r)
		str_arr_nuke (&client_argv);
	if (close (client_fd)) {
		msg (np, "\
In the parent, couldn't close the client's file descriptor");
		str_arr_nuke (&client_argv);
		return 1;
	}
	return r;
}

nod static err_e write_to_save_file () {
	int r = 0;
	if (lseek (CACHE_FD, 0, SEEK_SET) == -1)
		WARN_FAIL ("Couldn't seek to start of cache file");
	if (n_fs_write  (CACHE_FD, STATE, sizeof *STATE) == -1)
		msg (np, "Couldn't write cache file");
	return r;
}

static void cleanup () {
	if (getpid () != MAIN_PID)
		return;
	if (thrd_equal (MAIN_THREAD, thrd_current ()))
		thrd_join (CHECK_TIMERS_THREAD, 0);

	kill_all_children (&CHILDREN);
	if (write_to_save_file ()) {
		// To be honest, do nothing.
	}
}

static void signal_handler (int signal) {
	cleanup ();
	exit (0);
}

[[noreturn]]
static void exit_server () {
	STATE->kill = 0;
	cleanup ();
	exit (SUCC);
}

nod static err_e poll_for_and_deal_with_clients (void *) {

	// Set up the server fd for poll to poll for new client
	// connections.

	for (;;) {

		// These are debug messages, not to stick around forever.
#if 0
		fprintf (stderr, "n_children: %ld\n", children.n);
		FORI (i, children.n) {
			FORI (j, children.d[i].argv.n)
				fprintf (stderr, "%s ", children.d[i].argv.d[j]);
			n_fs_newline_f (stderr);
		}
#endif
		if (STATE->kill)
			exit_server ();
		STATE->now = ts_time_from_ts (time (0));

		struct pollfd server_pollfd = {SERVER_FD, POLLIN, 0};

		int n_incoming_server_connections = poll (&server_pollfd, 1, 1000);

		if (n_incoming_server_connections == -1) {
			if (errno != EINTR)
				WARN_FAIL ("Failed while polling");
			errno = 0;
		}

		else if (server_pollfd.revents & POLLIN) {

			FORI (i, n_incoming_server_connections)

				// DEDGEC.
				(void) deal_with_client ();
		}

		// DEDGEC: I don't know what to do if wait_for_children fails. I'll
		// cast it to void in case I ever develop an opinion and grep
		// for it.
		(void) wait_for_children (&CHILDREN);
	}
}

nod static int check_timers (void *) {

	int r = 0;
	for (;;) {
		if (STATE->kill)
			break;
		auto timers = &STATE->timers;

		// A bodge. We don't want to spam stop-alarm messages.
		bool did_stop_alarm = 0;
		EACH (timer, timers->n, timers->d) {

			if (!did_stop_alarm && (timer->flags & TF_stop_alarm)) {

				// DEDGEC.
				(void) for_all_cmd_runners (timer, stop_alarm);
				did_stop_alarm = 1;
				timer->flags &= ~TF_stop_alarm;
			}

			// This is bad behaviour. Masking a bug. Or maybe it's
			// very intelligent behaviour. The problem is, I guess,
			// there's chance the child can add a timer a that's
			// zeroed? Don't get it but I've been at this for a long
			// time and have finally got it working. So I don't care!
			if (timer->end.ts == 0)
				continue;

			// Pausing is just adding one every second. I don't think
			// that's an incredible way to do it. Really, while
			// paused, all processing, should be skipped, and when
			// it's unpaused we recalculate.
			if (timer->flags & TF_paused)
				timer->end.ts += 1;

			time_t remaining = timer->end.ts - STATE->now.ts;

			if (!(timer->flags & TF_completed) && remaining < 0) {
				r = for_all_cmd_runners (timer, do_alarm);
				if (r)
					msg (np, "\
An alarm for-all-cmd-runners failed. There should be lower-down message and not this one");
				if (*timer->cmd) {
					r = for_all_cmd_runners (timer, exec_cmd);
					if (r)
						msg (np, "\
A cmd for-all-cmd-runners failed. There should be lower-down message and not this one");
				}
				if (!(timer->flags & TF_repeat_tomorrow)) {
					timer->flags |= TF_completed;
					timer->flags &= ~TF_locked;
					if (STATE->completed_timers_retention_dur != -1 &&
							remaining < -STATE->completed_timers_retention_dur)
						splice_timer (timer - timers->d, 1);
				} else {
					while (timer->end.ts < STATE->now.ts)
						timer->end.ts += 60 * 60 * 24;
				}
			}
		}
		sleep (1);
	}
	return r;
}

nod static int glob_errfunc (const char *epath, int eerrno) {
	msg (np, "Glob failed on %s", epath);
	return 1;
}

nod static err_e get_shortcut_tune (int cfd, size_t n_pr, char *pr, bool exact_only) {

	err_e r = FAIL;
	char saved_dir[PATH_MAX];
	getcwd (saved_dir, PATH_MAX);

	chdir (STATE->data_dir);

	// Else, we try globbing src "name*".
	char buf[PATH_MAX];
	snprintf (buf, PATH_MAX, "%s.*", DEFAULT_TUNE_BASE_BASENAME);

	glob_t p_glob = {};
	if (glob (buf, 0, glob_errfunc, &p_glob)) {
		msg (np, "glob failed");
		goto out;
	}

	// Make sure we've got exactly one match.
	if (!p_glob.gl_pathc) {
		msg (np, "%s isn't in %s", buf, STATE->data_dir);
		goto out;
	} else if (p_glob.gl_pathc > 1) {
		msg (np, "Too many matches for %s in %s", buf, STATE->data_dir);
		goto out;
	}

	int rc = snprintf (pr, n_pr, "%s/%s", STATE->data_dir, p_glob.gl_pathv[0]);
	if (rc >= n_pr) {
		msg (&(mo_s) {.fd = cfd}, "Your shortcut tune's name is way too big");
		goto out;
	}
	else if (rc == -1) {
		msg (&(mo_s) {.fd = cfd}, "Couldn't create your shortcut tune name");
		goto out;
	}

	globfree (&p_glob);
	r = SUCC;
out:

	// FIXME: what to do if you can't CD back? You'd really need to
	// try again.
	if (chdir (saved_dir)) {
		msg (np, "couldn't chdir back to %s", saved_dir);
		return FAIL;
	}
	return r;
}

static void sigchild_handler (int) {
}

nod static err_e set_signal_handlers () {
	if (sigaction (SIGINT, &(struct sigaction) {{signal_handler}}, 0))
		WARN_FAIL ("Couldn't install sigint handler");
	if (sigaction (SIGTERM, &(struct sigaction) {{signal_handler}}, 0))
		WARN_FAIL ("Couldn't install sigterm handler");
	if (sigaction
		(SIGCHLD, &(struct sigaction) {.sa_handler = sigchild_handler}, 0))
		WARN_FAIL ("Couldn't install signal handler");
	return SUCC;
}

nod static err_e open_cache_file () {
	char *home = getenv ("HOME");
	ASSERT (home);
	char cache_path[BUFSIZ];
	sprintf (cache_path, "%s/.cache/timer", home);

	bool file_exists = n_fs_fexists (cache_path);

	// It's sufficient to check for errno since fexists unsets ENOENT.
	if (errno)
		WARN_FAIL ("Couldn't access cache file %d", errno);
	errno = 0;
	CACHE_FD = open (cache_path, O_CREAT | O_RDWR, 0750);
	if (CACHE_FD == -1)
		WARN_FAIL ("Couldn't make cache file %s", cache_path);

	if (file_exists) {
		if (read (CACHE_FD, STATE, sizeof *STATE) == -1)
			WARN_FAIL ("Couldn't read in cached timers");
	}

	return SUCC;
}

nod err_e init_shm (char *shm_file_basename) {

	int shm_fd = shm_open (shm_file_basename, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	if (shm_fd == -1)
		WARN_FAIL ("Couldn't create shared memory: %m");
	if (ftruncate (shm_fd, sizeof *STATE))
		WARN_FAIL ("Couldn't truncate shared memory: %m");
	STATE = mmap (0, sizeof *STATE, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
	if (STATE == MAP_FAILED)
		WARN_FAIL ("Couldn't mmap shared memory: %m");
	if (close (shm_fd))
		WARN_FAIL ("Couldn't close shared memory file descriptor: %m");

	*STATE = (state_s) {
		.now = ts_time_from_ts (time (0)),
		.tune_duration = -1,
	};

	get_data_dir (STATE->data_dir);

	// FIXME: this obviously will always be the case.
	if (!*STATE->tune) {
		int rc;
		rc = get_shortcut_tune (-1, PATH_MAX, STATE->tune, 1);
		if (rc)
			exit (1);
		if (!*STATE->tune)
			msg (np, "\
You have no default tune in %s. You should stick one there and call it \
something that matches \"%s\" \
-- or use -T or -t", STATE->data_dir, DEFAULT_TUNE_BASE_BASENAME);
	} else if (!n_fs_fexists (STATE->tune)) {
		if (errno)
			WARN_FAIL ("%s: %m", STATE->tune);
		else
			WARN_FAIL ("%s doesn't exist", STATE->tune);
	}
	STATE->completed_timers_retention_dur = DEFAULT_COMPLETED_TIMERS_RETENTION_DUR;
	return SUCC;
}

nod err_e server_run (bool daemonise) {

	if (make_enforce_single_instance (EXE_NAMES[EXE_server], &LOCK_FILE_PID))
		return FAIL;
	snprintf (SHM_FILE_PATH_TAIL, PATH_MAX, "/timer-%s", EXE_NAMES[EXE_server]);
	if (init_shm (SHM_FILE_PATH_TAIL))
		return FAIL;
	if (set_signal_handlers ())
		return FAIL;
	if (open_cache_file ())
		return FAIL;
	if (create_server_socket (SERVER_PORT, &SERVER_FD))
		return FAIL;

	if (daemonise && daemon (0, 0))
		WARN_FAIL ("Couldn't daemonise");

	// threads funcs return an enum, with values like "thrd_success",
	// and they don't specify what these values are. But I'm still
	// going to assume that thrd_success is 0.
	if (thrd_create (&CHECK_TIMERS_THREAD, check_timers, 0))
		WARN_FAIL ("Couldn't create timer-checking thread");

	return poll_for_and_deal_with_clients (np);
}

nod err_e create_server_socket (int port, int *fd) {

	*fd = socket (AF_INET, SOCK_STREAM, 0);
	if (*fd == -1)
		WARN_FAIL ("Couldn't create socket");

	struct sockaddr_in addr = {
		.sin_family = AF_INET,
		.sin_port = htons (port),
		.sin_addr = {.s_addr = INADDR_ANY},
	};

	int optval = 1;

	// I've read a bit about the TIME_WAIT state now and am not sure
	// whether this is sensible. Basically, you get a TIME_WAIT state
	// and it lasts 2 * the maximum segment size. The reason we
	// want this timeout is something to do with how packets might get
	// lost and then turn up. It confuses the issue if you've
	// connected again and then your old packet turns up. So maybe we
	// shouldn't set this option. Still, how the fuck would you debug
	// the program if you didn't have it set? Two times the MSS
	// can be up to four minutes.
	//
	// ChatGPT tells me you could use different ports. That makes
	// sense. Though the clients would have to know those, so it
	// doesn't.
	if (setsockopt (*fd, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof optval))
		msg (np, "\
Couldn't make the socket reusable, meaning you might not be able to reconnect right \
away if you stop the server");

	if (bind (*fd, (struct sockaddr *) &addr, sizeof addr))
		WARN_FAIL ("Couldn't bind the socket");

	if (listen (*fd, 10000))
		WARN_FAIL ("Couldn't listen on the socket");

	return SUCC;
}

nod int server_accept_client (int server_fd) {

	// In truth there's nothing about this fn that's really about
	// clients.

	struct sockaddr_in client_addr = {};
	socklen_t client_addr_size = 0;
	int cfd = accept (server_fd, (struct sockaddr *) &client_addr, &client_addr_size);

	if (cfd == -1 && errno && errno != EAGAIN && errno != EWOULDBLOCK) {
		msg (np, "Couldn't accept connection");
		return -1;
	}

	return cfd;
}
