#define _GNU_SOURCE

#include "all.h"
#include "warn.h"
#include "macros.h"
#include "client.h"
#include <limits.h>
#include <stdarg.h>
#include <fcntl.h>
#include <sys/file.h>
#include <errno.h>

char *PROG_NAME;
pid_t MAIN_PID;
int CACHE_FD;
char *EXE_NAMES[EXE_N] = { EXE_DEFS (SEL_2) };

nod err_e make_enforce_single_instance (char *service_name, int *fd) {
	char lock_file_path[PATH_MAX];
	snprintf (lock_file_path, PATH_MAX, "/tmp/timer-%s.lock", service_name);
	*fd = open (lock_file_path, O_CREAT | O_RDWR, 0666);
	if (*fd == -1)
		WARN_FAIL ("Couldn't create/open PID file: %m");

	err_e r = FAIL;
	if (flock (r, LOCK_EX | LOCK_NB)) {
		if (errno == EWOULDBLOCK)
			WARN_OUT ("Another timer server is already running");
	}

	r = SUCC;
out:
	return r;
}


