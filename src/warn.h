#pragma once

typedef enum {
	SUCC,
	FAIL,
} err_e;

typedef struct msg_opts mo_s;
struct msg_opts {
	char *flags;
	int fd;
};

#define WARN_RET($code, $fmt, ...) \
	do { \
		warn ($fmt __VA_OPT__(,) __VA_ARGS__);  \
		return $code; \
	} while (false)
#define WARN_FAIL($fmt, ...) \
	WARN_RET (FAIL, $fmt __VA_OPT__(,) __VA_ARGS__)
#define WARN_SUCC($fmt, ...) \
	WARN_RET (SUCC, $fmt __VA_OPT__(,) __VA_ARGS__)
#define WARN_OUT($fmt, ...) \
	do { \
		warn ($fmt __VA_OPT__(,) __VA_ARGS__);  \
		goto out; \
	} while (false)

void warn (char *fmt, ...);
void msg (mo_s *msg_opts, char *fmt, ...);
