#define _GNU_SOURCE

#include "audio.h"
#include "so-called-protocol.h"
#include "child.h"
#include "all.h"
#include "server.h"
#include "state.h"
#include "cmd-runner.h"
#include "warn.h"
#include <stdlib.h>
#include <string.h>
#include <poll.h>
#include <stdio.h>
#include <sys/wait.h>

char *ALARM_CMD      = "alarm";
char *STOP_ALARM_CMD = "stop-alarm";
char *CMD_CMD        = "cmd";

static int SERVER_FD;
static child_arr_s CHILDREN;

static audio_ctx_s *AUDIO_CTX;

nod int exec_add_pid (char *cmd, time_t run_for) {
	str_arr_s argv = {};

	DARR_GROW_ADD (&argv, 1, 1, strdup ("sh"));
	DARR_GROW_ADD (&argv, 1, 1, strdup ("-c"));

	// Always strdup; keep it simple.
	DARR_GROW_ADD (&argv, 1, 1, strdup (cmd));
	DARR_GROW_ADD (&argv, 1, 1, np);

	int r = fork ();
	if (r == -1)
		return r;
	else if (!r) {
		r = execvp ("sh", argv.d);
		if (r)
			msg (np, "Couldn't execute %s", cmd);
		return 1;
	}

	DARR_GROW_ADD (&CHILDREN, 1, 1,
			(child_s) {
			 	.pid = r,
				.argv = argv,
				.end = run_for == -1 ? -1 : STATE->now.ts + run_for
			});
	return 0;
}

nod static int do_alarm (char *timer_name, time_t tune_dur, char *tune_path) {

	int rc;
	rc = audio_queue_add_to_playing (AUDIO_CTX, tune_path);
	if (rc) {
		msg (np, "Couldn't play %s: %s", tune_path, AUDIO_CTX->err_buf);
		return 1;
	}

	char notify_send_cmdline[256];
	snprintf (notify_send_cmdline, sizeof notify_send_cmdline,
			"notify-send Timer \"%s complete\"", timer_name);

	int r = exec_add_pid (notify_send_cmdline, -1);
	if (r)
		return r;

	return r;
}

nod static int stop_alarms () {
	int r = audio_queue_stop_playing_tunes (AUDIO_CTX);
	if (r)
		msg (np, "Couldn't stop alarms: %s", AUDIO_CTX->err_buf);
	return r;
}

nod static int deal_with_client () {
	int client_fd = server_accept_client (SERVER_FD);
	if (client_fd == -1) {
		msg (np, "Couldn't accept client");
		return 1;
	}
	str_arr_s cmd = {};
	int r = protocol_get_argv_from_fd_stream (&cmd, client_fd);
	if (r) {
		msg (np, "Couldn't read anything");
		goto out;
	}

	if (!strcmp (cmd.d[0], ALARM_CMD)) {
		time_t tune_dur = strtol (cmd.d[2], np, 0);
		r = do_alarm (cmd.d[1], tune_dur, cmd.d[3]);
	} else if (!strcmp (cmd.d[0], STOP_ALARM_CMD)) {
		r = stop_alarms ();
	} else { // It's a "cmd"

		int child_pid = fork ();
		if (child_pid == -1) {
			msg (np, "Couldn't fork");
			goto out;

		} else if (child_pid == 0) {
			printf ("Running %s\n", return_strs_as_static_string (&cmd, 1));
			int client_r = exec_add_pid (cmd.d[1], -1);
			exit (client_r);
		}

		DARR_GROW_ADD (&CHILDREN, 1, 1,
				(child_s) {
				.pid = child_pid,
				.argv = cmd,
				.end = -1
			});
	}

out:
	return r;
}

static void poll_for_and_deal_with_clients () {
	while (true) {
		struct pollfd pollfd = {SERVER_FD, POLLIN, 0};
		int n_incoming_server_connections = poll (&pollfd, 1, -1);
		if (n_incoming_server_connections == -1)
			msg (np, "Failed while polling");
		else if (pollfd.revents & POLLIN) {
			FORI (i, n_incoming_server_connections)
				(void) deal_with_client ();
		}
		ASSERT (!wait_for_children (&CHILDREN));
	}
}

nod static err_e cleanup () {
	if (getpid () != MAIN_PID)
		return SUCC;

	// Who knows if this really does anything.
	(void) audio_queue_quit (AUDIO_CTX);

	kill_all_children (&CHILDREN);
	return SUCC;
}

static void atexit_func () {
	(void) cleanup ();
}

static void signal_handler (int signal) {
	(void) cleanup ();
	exit (signal);
}

nod static err_e set_signal_handlers () {
	if (sigaction (SIGINT, &(struct sigaction) {{signal_handler}}, 0))
		WARN_FAIL ("Couldn't install sigint handler");
	if (sigaction (SIGTERM, &(struct sigaction) {{signal_handler}}, 0))
		WARN_FAIL ("Couldn't install sigterm handler");
	if (sigaction (SIGCHLD, &(struct sigaction) {{SIG_IGN}}, 0))
		WARN_FAIL ("Couldn't install signal handler");
	return SUCC;
}

nod err_e cmd_runner (bool daemonise) {
	{
		int fd;
		if (make_enforce_single_instance (EXE_NAMES[EXE_cmd_runner], &fd))
			return FAIL;
	}
	AUDIO_CTX = audio_init ("Timer cmd-runner alarm", AUDIO_SAMPLE_RATE, 2);
	if (!AUDIO_CTX->valid)
		WARN_FAIL ("Couldn't start audio: %s", AUDIO_CTX->err_buf);
	atexit (atexit_func);
	if (set_signal_handlers ())
		return FAIL;
	if (create_server_socket (CMD_RUNNER_PORT, &SERVER_FD))
		return FAIL;
	if (daemonise && daemon (0, 0))
		WARN_FAIL ("Couldn't daemonise");

	poll_for_and_deal_with_clients ();
	return SUCC;
}
