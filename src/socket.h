#pragma once

#include <sys/socket.h>

int connect_with_timeout (int sockfd, const struct sockaddr *addr, socklen_t addrlen, unsigned int timeout_ms);
