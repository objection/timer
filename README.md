# Timer

## Install

First, install meson. Then:

```
git clone --recursive https://gitlab.com/objection/timer.git
meson build
sudo meson build install
```

You'll probably want to open port 8884 (for the server) and port 8885
(for the "cmd-runner"; see below). You'll want those two programs
running at boot; two systemd service files are provided, and there's a
script (install-services.sh) to install them.

## How it works

It's a client server thing. You can run the server with "timer
--be-server". Use --no-daemonise to run it in the
foreground.

The help explains things reasonably well. To get started, all you
really need to know is `timer 1h my-timer`. The "my-timer" is the name
of the timer, and it's optional. The "1h" is the timer's duration.
It's just a list of digits followed by units, eg, "1h40m20s". The
units are:

* h: hours
* m: minutes
* s: seconds
* d: days
* w: weeks
* M: months
* y: years

Some options are "actions", like --delete. These action options
operate on the notes you select with --select (-s). --select takes
what I unimaginatively persist in calling an "int list", a string
like this: "0", "0..-1", "-1". -1 is the end of the list, 0 is the
start; two dots means produce a range between the bookending digits
(including the last), a
comma means ... An example will help: "0,3" means, 0 and 3, ie, the
first and fourth timers.

In order to hear alarms and run commands you'll need to run, believe
it or not, a second server, the "cmd-runner", which you invoke like
this: "timer --be-cmd-runner". Like the server it daemonises itself
by default; -D foregrounds it.

# systemd Service files

To install these, you can run the script install-services.sh, which
takes one argument, your username, like this `sudo
./install-service.sh $USER`. Or you change their "user"
and "group" fields to your own user and group, copy them to
/etc/systemd/system and type

```
sudo systemctl enable --now timer.service
sudo systemctl enable --now timer-cmd-runner.service
```

You could also install them as "user services", by sticking them in
~/.local/systemd/user and using doing `systemctl --user`, but I've had
bad luck with that.
